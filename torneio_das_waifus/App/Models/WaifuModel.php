<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use App\Models\Entidades\Waifu;
use App\Models\Entidades\InscricaoDaWaifu;
use App\Models\Entidades\Torneio;

final class WaifuModel{

  public function selecionar_waifu($pk_waifu){
    $query = DB::table('waifu');
    $query = $query->addSelect('pk_waifu');
    $query = $query->addSelect('nome');
    $query = $query->addSelect('imagem');
    $query = $query->where('pk_waifu', '=', $pk_waifu);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    if(count($array_resultado) === 0){
      $mensagem_do_model = "Nenhuma waifu com ID $pk_waifu foi encontrada no banco de dados";
      $mensagem_do_model .= ' do sistema.';
      $array_resultado['mensagem_do_model'] = $mensagem_do_model;
    }else{
      $array_melhorado = array();
      foreach($array_resultado as $objeto_generico){
        $array_waifu = (array) $objeto_generico;
        $waifu = new Waifu($array_waifu);
        $array_melhorado[] = $waifu;
      }
      $array_resultado = $array_melhorado;
    }

    return $array_resultado;
  }

  public function selecionar_inscricoes($pk_waifu, $filtros, $ordenacao, $quantidade, $descartar){
    $query = DB::table('inscricao_da_waifu');
    $query = $query->addSelect('pk_inscricao_da_waifu');
    $query = $query->addSelect('inscricao_da_waifu.fk_torneio AS pk_torneio');
    $query = $query->addSelect('inscricao_da_waifu.etapa AS etapa_da_inscricao');
    $query = $query->addSelect('torneio.nome');
    $query = $query->addSelect('torneio.quantidade_de_waifus');
    $query = $query->addSelect('torneio.status');

    $query = $query->where('inscricao_da_waifu.fk_waifu', '=', $pk_waifu);

    $query = $query->join('torneio', 'inscricao_da_waifu.fk_torneio', '=', 'pk_torneio'); //INNER JOIN

    foreach($filtros as $chave => $valor){
      switch($chave){
        case 'nome_do_torneio':
          $query = $query->where('torneio.nome', 'LIKE', "%$valor%");
          break;
        case 'tipo_do_torneio':
          $query = $query->where('torneio.quantidade_de_waifus', '=', $valor);
          break;
        case 'status_do_torneio':
          $query = $query->where('torneio.status', '=', $valor);
          break;
      }
    }

    switch($ordenacao){
      case 'padrao':
        $query = $query->orderBy('pk_inscricao_da_waifu', 'DESC');
        break;
      case 'nome_a_z':
        $query = $query->orderBy('torneio.nome', 'ASC');
        break;
      case 'nome_z_a':
        $query = $query->orderBy('torneio.nome', 'DESC');
        break;
      case 'tipo_crescente':
        $query = $query->orderBy('torneio.quantidade_de_waifus', 'ASC');
        $query = $query->orderBy('pk_inscricao_da_waifu', 'DESC');
        break;
      case 'tipo_decrescente':
        $query = $query->orderBy('torneio.quantidade_de_waifus', 'DESC');
        $query = $query->orderBy('pk_inscricao_da_waifu', 'DESC');
        break;
      case 'status_crescente':
        $query = $query->orderBy('torneio.status', 'ASC');
        $query = $query->orderBy('pk_inscricao_da_waifu', 'DESC');
        break;
      case 'status_decrescente':
        $query = $query->orderBy('torneio.status', 'DESC');
        $query = $query->orderBy('pk_inscricao_da_waifu', 'DESC');
        break;
      case 'etapa_crescente':
        $query = $query->orderByRaw('IF(torneio.status=\'aguardando_inscricoes\', 0, 1) ASC');
        $query = $query->orderByRaw('inscricao_da_waifu.etapa - (torneio.quantidade_de_waifus + 1) ASC'); //Propositalmente eu não pretendo colocar essa parte no else do if acima.
        $query = $query->orderBy('torneio.quantidade_de_waifus', 'ASC');
        $query = $query->orderBy('pk_inscricao_da_waifu', 'DESC');
        break;
      case 'etapa_decrescente':
        $query = $query->orderByRaw('IF(torneio.status=\'aguardando_inscricoes\', 0, 1) DESC');
        $query = $query->orderByRaw('inscricao_da_waifu.etapa - (torneio.quantidade_de_waifus + 1) DESC'); //Propositalmente eu não pretendo colocar essa parte no else do if acima.
        $query = $query->orderBy('torneio.quantidade_de_waifus', 'DESC');
        $query = $query->orderBy('pk_inscricao_da_waifu', 'DESC');
        break;
      //Dica:
      //(torneio.quantidade_de_waifus + 1) é o mesmo que:
      //(LOG(2, CAST(CAST(torneio.quantidade_de_waifus AS CHAR) AS UNSIGNED)) + 1)
      //pois a coluna quantidade_de_waifus é ENUM('2', '4', '8', '16', '32').
      //O cálculo do logarítimo + 1 é o cálculo da etapa máxima do torneio.
    }

    $query = $query->offset($descartar);
    $query = $query->limit($quantidade);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado = array();
    foreach($array_resultado as $objeto_generico){
      $array_valores_do_banco_de_dados = (array) $objeto_generico;

      $inscricao_da_waifu = new InscricaoDaWaifu();
      $inscricao_da_waifu->set_pk_inscricao_da_waifu($array_valores_do_banco_de_dados['pk_inscricao_da_waifu']);
      $inscricao_da_waifu->set_etapa($array_valores_do_banco_de_dados['etapa_da_inscricao']);

      $torneio = new Torneio();
      $torneio->set_pk_torneio($array_valores_do_banco_de_dados['pk_torneio']);
      $torneio->set_nome($array_valores_do_banco_de_dados['nome']);
      $torneio->set_quantidade_de_waifus($array_valores_do_banco_de_dados['quantidade_de_waifus']);
      $torneio->set_status($array_valores_do_banco_de_dados['status']);

      $inscricao_da_waifu->set_torneio($torneio);

      $array_melhorado[] = $inscricao_da_waifu;
    }
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

  public function contar_inscricoes($pk_waifu, $filtros){
    $query = DB::table('inscricao_da_waifu');
    $query = $query->select(DB::raw('COUNT(*) AS quantidade'));
    $query = $query->where('fk_waifu', '=', $pk_waifu);

    $query = $query->join('torneio', 'inscricao_da_waifu.fk_torneio', '=', 'pk_torneio'); //INNER JOIN

    foreach($filtros as $chave => $valor){
      switch($chave){
        case 'nome_do_torneio':
          $query = $query->where('torneio.nome', 'LIKE', "%$valor%");
          break;
        case 'tipo_do_torneio':
          $query = $query->where('torneio.quantidade_de_waifus', '=', $valor);
          break;
        case 'status_do_torneio':
          $query = $query->where('torneio.status', '=', $valor);
          break;
      }
    }

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado['quantidade'] = $array_resultado[0]->quantidade;
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

}
