<?php

use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

/* Página Padrão */
Route::get('/', [App\Http\Controllers\InicioController::class, 'carregar_pagina']);

/* Início */
Route::get('/inicio', [App\Http\Controllers\InicioController::class, 'carregar_pagina']);

/* Nova Waifu */
Route::get('/nova_waifu', [App\Http\Controllers\NovaWaifuController::class, 'carregar_pagina']);
Route::post('/nova_waifu/cadastrar_waifu', [App\Http\Controllers\NovaWaifuController::class, 'cadastrar_waifu']);

/* Editar Waifu */
Route::get('/editar_waifu', [App\Http\Controllers\EditarWaifuController::class, 'carregar_pagina']);
Route::post('/editar_waifu/editar_waifu', [App\Http\Controllers\EditarWaifuController::class, 'editar_waifu']);

/* Inscrever Waifu */
Route::get('/inscrever_waifu', [App\Http\Controllers\InscreverWaifuController::class, 'carregar_pagina']);
Route::get('/inscrever_waifu/mostrar_torneios_que_estao_aguardando_inscricoes_ajax', [App\Http\Controllers\InscreverWaifuController::class, 'mostrar_torneios_que_estao_aguardando_inscricoes_ajax']);
Route::get('/inscrever_waifu/inscrever', [App\Http\Controllers\InscreverWaifuController::class, 'inscrever']);

/* Novo Torneio */
Route::get('/novo_torneio', [App\Http\Controllers\NovoTorneioController::class, 'carregar_pagina']);
Route::post('/novo_torneio/cadastrar_torneio', [App\Http\Controllers\NovoTorneioController::class, 'cadastrar_torneio']);
Route::post('/novo_torneio/buscar_pelo_nome_da_waifu_ajax', [App\Http\Controllers\NovoTorneioController::class, 'buscar_pelo_nome_da_waifu_ajax']);

/* Waifus */
Route::get('/waifus', [App\Http\Controllers\WaifusController::class, 'carregar_pagina']);
Route::get('/waifus/mostrar_waifus_ajax', [App\Http\Controllers\WaifusController::class, 'mostrar_waifus_ajax']);

/* Waifu */
Route::get('/waifu', [App\Http\Controllers\WaifuController::class, 'carregar_pagina']);
Route::get('/waifu/mostrar_inscricoes_ajax', [App\Http\Controllers\WaifuController::class, 'mostrar_inscricoes_ajax']);

/* Torneios */
Route::get('/torneios', [App\Http\Controllers\TorneiosController::class, 'carregar_pagina']);
Route::get('/torneios/mostrar_torneios_que_estao_aguardando_inscricoes_ajax', [App\Http\Controllers\TorneiosController::class, 'mostrar_torneios_que_estao_aguardando_inscricoes_ajax']);
Route::get('/torneios/mostrar_torneios_iniciados_ajax', [App\Http\Controllers\TorneiosController::class, 'mostrar_torneios_iniciados_ajax']);
Route::get('/torneios/mostrar_torneios_encerrados_ajax', [App\Http\Controllers\TorneiosController::class, 'mostrar_torneios_encerrados_ajax']);

/* Torneio */
Route::get('/torneio', [App\Http\Controllers\TorneioController::class, 'carregar_pagina']);
Route::post('/torneio/votar_na_waifu', [App\Http\Controllers\TorneioController::class, 'votar_na_waifu']);
