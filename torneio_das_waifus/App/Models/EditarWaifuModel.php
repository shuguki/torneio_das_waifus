<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use App\Models\Entidades\Waifu;
use Exception;

final class EditarWaifuModel{

  public function selecionar_waifu($pk_waifu){
    $query = DB::table('waifu');
    $query = $query->addSelect('pk_waifu');
    $query = $query->addSelect('nome');
    $query = $query->addSelect('imagem');
    $query = $query->where('pk_waifu', '=', $pk_waifu);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    if(count($array_resultado) === 0){
      $mensagem_do_model = "Nenhuma waifu com ID $pk_waifu foi encontrada no banco de dados";
      $mensagem_do_model .= ' do sistema.';
      $array_resultado['mensagem_do_model'] = $mensagem_do_model;
    }else{
      $array_melhorado = array();
      foreach($array_resultado as $objeto_generico){
        $array_waifu = (array) $objeto_generico;
        $waifu = new Waifu($array_waifu);
        $array_melhorado[] = $waifu;
      }
      $array_resultado = $array_melhorado;
    }

    return $array_resultado;
  }

  public function verificar_disponibilidade_de_nome($pk_waifu, $nome){
    $query = DB::table('waifu');
    $query = $query->addSelect('nome');
    $query = $query->where('pk_waifu', '<>', $pk_waifu);
    $query = $query->where('nome', '=', $nome);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    if(count($array_resultado) > 0){
      $mensagem_do_model = 'O nome escolhido já é o nome de uma waifu cadastrada anteriormente.';
      $mensagem_do_model .= ' Por favor, utilize outro nome para editar esta waifu.';
      $array_resultado['mensagem_do_model'] = $mensagem_do_model;
    }

    return $array_resultado;
  }

  public function editar_waifu($waifu){
    $update['nome'] = $waifu->get_nome();
    $update['imagem'] = $waifu->get_imagem();

    $array_resultado = array();
    try{
      DB::table('waifu')->where('pk_waifu', '=', $waifu->get_pk_waifu())->update($update);
    }catch(Exception $excecao){
      $codigo_da_excecao = $excecao->getCode();
      switch($codigo_da_excecao){
        case 1062:
          $mensagem_do_model = 'Já existe uma waifu cadastrada com uma ou mais destas informações.';
          $array_resultado['mensagem_do_model'] = $mensagem_do_model;
          break;
        default:
          $array_resultado['mensagem_do_model'] = $excecao->getMessage();
          break;
      }
    }

    return $array_resultado;
  }

}
