<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use App\Models\Entidades\Waifu;
use App\Models\Entidades\Torneio;
use App\Models\Entidades\InscricaoDaWaifu;
use DateTime;
use DateTimeZone;
use Exception;

final class InscreverWaifuModel{

  public function selecionar_waifu($pk_waifu){
    $query = DB::table('waifu');
    $query = $query->addSelect('pk_waifu');
    $query = $query->addSelect('nome');
    $query = $query->addSelect('imagem');
    $query = $query->where('pk_waifu', '=', $pk_waifu);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    if(count($array_resultado) === 0){
      $mensagem_do_model = 'A waifu escolhida não se encontra no banco de dados do sistema.';
      $array_resultado['mensagem_do_model'] = $mensagem_do_model;
    }else{
      $array_waifu = (array) $array_resultado[0];
      $array_resultado[0] = new Waifu($array_waifu);
    }

    return $array_resultado;
  }

  public function selecionar_torneios_que_estao_aguardando_inscricoes($pk_waifu, $filtros, $ordenacao, $quantidade, $descartar){
    $query = DB::table('torneio');
    $query = $query->select(
      'pk_torneio',
      'nome',
      'quantidade_de_waifus',
      DB::raw('(SELECT COUNT(*) FROM inscricao_da_waifu WHERE fk_torneio = pk_torneio) AS quantidade_de_waifus_inscritas')
    );
    $query = $query->where('status', '=', 'aguardando_inscricoes');

    foreach($filtros as $chave => $valor){
      switch($chave){
        case 'nome_do_torneio':
          $query = $query->where('nome', 'LIKE', "%$valor%");
          break;
      }
    }

    switch($ordenacao){
      case 'padrao':
        $query = $query->orderBy('pk_torneio', 'DESC');
        break;
      case 'nome_a_z':
        $query = $query->orderBy('nome', 'ASC');
        break;
      case 'nome_z_a':
        $query = $query->orderBy('nome', 'DESC');
        break;
      case 'vagas_disponiveis_crescente':
        $query = $query->orderByRaw('IF(CAST(CAST(quantidade_de_waifus AS CHAR) AS UNSIGNED) - quantidade_de_waifus_inscritas < 0, 0, CAST(CAST(quantidade_de_waifus AS CHAR) AS UNSIGNED) - quantidade_de_waifus_inscritas) ASC');
        $query = $query->orderBy('pk_torneio', 'DESC');
        break;
      case 'vagas_disponiveis_decrescente':
        $query = $query->orderByRaw('IF(CAST(CAST(quantidade_de_waifus AS CHAR) AS UNSIGNED) - quantidade_de_waifus_inscritas < 0, 0, CAST(CAST(quantidade_de_waifus AS CHAR) AS UNSIGNED) - quantidade_de_waifus_inscritas) DESC');
        $query = $query->orderBy('pk_torneio', 'DESC');
        break;
      case 'opcoes_a_z':
        $query = $query->orderByRaw('IF((SELECT COUNT(*) FROM inscricao_da_waifu WHERE fk_waifu=? AND fk_torneio=pk_torneio) > 0, \'Já está inscrita\', \'Inscrever\') ASC', $pk_waifu);
        $query = $query->orderBy('pk_torneio', 'DESC');
        break;
      case 'opcoes_z_a':
        $query = $query->orderByRaw('IF((SELECT COUNT(*) FROM inscricao_da_waifu WHERE fk_waifu=? AND fk_torneio=pk_torneio) > 0, \'Já está inscrita\', \'Inscrever\') DESC', $pk_waifu);
        $query = $query->orderBy('pk_torneio', 'DESC');
        break;
    }

    $query = $query->offset($descartar);
    $query = $query->limit($quantidade);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado = array();
    foreach($array_resultado as $objeto_generico){
      $array_torneio = (array) $objeto_generico;
      $torneio = new Torneio($array_torneio);
      $array_melhorado[] = $torneio;
    }
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

  public function contar_torneios_que_estao_aguardando_inscricoes($filtros){
    $query = DB::table('torneio');
    $query = $query->select(DB::raw('COUNT(*) AS quantidade'));
    $query = $query->where('status', '=', 'aguardando_inscricoes');

    foreach($filtros as $chave => $valor){
      switch($chave){
        case 'nome_do_torneio':
          $query = $query->where('nome', 'LIKE', "%$valor%");
          break;
      }
    }

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado['quantidade'] = $array_resultado[0]->quantidade;
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

  public function selecionar_torneio($pk_torneio){
    $query = DB::table('torneio');
    $query = $query->addSelect('pk_torneio');
    $query = $query->addSelect('nome');
    $query = $query->addSelect('quantidade_de_waifus');
    $query = $query->addSelect('status');
    $query = $query->addSelect('etapa');
    $query = $query->where('pk_torneio', '=', $pk_torneio);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    if(count($array_resultado) === 0){
      $mensagem_do_model = 'O torneio escolhido não se encontra no banco de dados do sistema.';
      $array_resultado['mensagem_do_model'] = $mensagem_do_model;
    }else{
      $array_torneio = (array) $array_resultado[0];
      $array_resultado[0] = new Torneio($array_torneio);
    }

    return $array_resultado;
  }

  public function verificar_se_a_waifu_ja_estava_inscrita($fk_waifu, $fk_torneio){
    $query = DB::table('inscricao_da_waifu');
    $query = $query->addSelect('pk_inscricao_da_waifu');
    $query = $query->where('fk_waifu', '=', $fk_waifu);
    $query = $query->where('fk_torneio', '=', $fk_torneio);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    if(count($array_resultado) !== 0){
      $mensagem_do_model = 'Esta waifu já havia sido inscrita no torneio escolhido.';
      $array_resultado['mensagem_do_model'] = $mensagem_do_model;
      $array_resultado['esta_inscrita'] = true;
    }else{
      $array_resultado['esta_inscrita'] = false;
    }

    return $array_resultado;
  }

  public function selecionar_inscricoes_do_torneio($fk_torneio){
    $query = DB::table('inscricao_da_waifu');
    $query = $query->addSelect('pk_inscricao_da_waifu');
    $query = $query->addSelect('fk_waifu');
    $query = $query->addSelect('fk_torneio');
    $query = $query->addSelect('etapa');
    $query = $query->addSelect('posicao_inicial');
    $query = $query->where('fk_torneio', '=', $fk_torneio);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado = array();
    foreach($array_resultado as $objeto_generico){
      $array_inscricao = (array) $objeto_generico;
      $inscricao = new InscricaoDaWaifu($array_inscricao);
      $array_melhorado[] = $inscricao;
    }
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

  public function realizar_inscricao($inscricao_da_waifu){
    $insert['fk_waifu'] = $inscricao_da_waifu->get_fk_waifu();
    $insert['fk_torneio'] = $inscricao_da_waifu->get_fk_torneio();
    $insert['etapa'] = 0;
    $insert['posicao_inicial'] = $inscricao_da_waifu->get_posicao_inicial();

    $array_resultado = array();
    try{
      DB::table('inscricao_da_waifu')->insert($insert);
    }catch(Exception $excecao){
      $codigo_da_excecao = $excecao->getCode();
      switch($codigo_da_excecao){
        default:
          $array_resultado['mensagem_do_model'] = $excecao->getMessage();
          break;
      }
    }

    return $array_resultado;
  }

  public function iniciar_torneio($pk_torneio){
    $update['etapa'] = 1;
    $update['status'] = 'torneio_iniciado';

    $sem_fuso_horario = new DateTimeZone('GMT');
    $objeto_date_time = new DateTime('now', $sem_fuso_horario);
    $update['momento_do_inicio'] = $objeto_date_time->format('Y-m-d H:i:s');

    $array_resultado = array();
    try{
      DB::table('torneio')->where('pk_torneio', '=', $pk_torneio)->update($update);
    }catch(Exception $excecao){
      $codigo_da_excecao = $excecao->getCode();
      switch($codigo_da_excecao){
        default:
          $array_resultado['mensagem_do_model'] = $excecao->getMessage();
          break;
      }
    }

    return $array_resultado;
  }

  public function atualizar_inscricoes_do_torneio($fk_torneio){
    $update['etapa'] = 1;

    $array_resultado = array();
    try{
      DB::table('inscricao_da_waifu')->where('fk_torneio', '=', $fk_torneio)->where('etapa', '<', 1)->update($update);
    }catch(Exception $excecao){
      $codigo_da_excecao = $excecao->getCode();
      switch($codigo_da_excecao){
        default:
          $array_resultado['mensagem_do_model'] = $excecao->getMessage();
          break;
      }
    }

    return $array_resultado;
  }

}
