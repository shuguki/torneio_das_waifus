<?php

namespace App\Http\Controllers;

use App\Models\WaifuModel;
use App\Models\Entidades\Torneio;
use Inertia\Inertia;

final class WaifuController extends TemplateLayoutController{
  private const QUANTIDADE_PADRAO_POR_PAGINA = 30;

  public function carregar_pagina($redirecionar_com_id = false){
    if($redirecionar_com_id !== false){
      //Redireciona para si mesmo, motivo: limpar a requisição.
      header("Location: /waifu?id=$redirecionar_com_id");
      die;
    }

    $valores = $this->valores_do_template_layout();
    $sessao = session();

    /* Colocando valores iniciais nas variáveis para não ficarem undefined no Vue */
    $valores['waifu']['mensagem'] = '';
    $valores['waifu']['id_valido'] = true;
    $valores['waifu']['id'] = '';
    $valores['waifu']['nome'] = '';
    $valores['waifu']['imagem'] = '';
    $torneio = new Torneio();
    $valores['waifu']['tipos_de_torneios'] = $torneio->enum_quantidade_de_waifus();
    $valores['waifu']['status_de_torneios'] = $torneio->enum_status();
    $valores['waifu']['inscricoes'] = array();

    $waifu_model = new WaifuModel();

    /* Validando o ID da waifu informado na URL */
    $requisicao = $this->get_requisicao();
    $pk_waifu = $requisicao->get('id');
    if(!is_numeric($pk_waifu) or $pk_waifu <= 0 or floor($pk_waifu) != $pk_waifu){
      $mensagem = 'ID inválido, o ID da waifu precisa ser um número natural maior que zero.';
      $valores['waifu']['mensagem'] = $mensagem;
      $valores['waifu']['id_valido'] = false;
    }else{
      /* Consultando e mostrando informações da waifu */
      $array_resultado = $waifu_model->selecionar_waifu($pk_waifu);
      if(isset($array_resultado['mensagem_do_model'])){
        $valores['waifu']['mensagem'] = $array_resultado['mensagem_do_model'];
        $valores['waifu']['id_valido'] = false;
      }else{
        $waifu = $array_resultado[0];
        $valores['waifu']['id'] = $waifu->get_pk_waifu();
        $valores['waifu']['nome'] = $waifu->get_nome();
        $valores['waifu']['imagem'] = $waifu->get_imagem();

        /* Chamando método para mostrar os torneios nos quais a waifu foi inscrita */
        $valores_inscricoes = $this->mostrar_inscricoes($pk_waifu);
        $valores['waifu']['inscricoes'] = $valores_inscricoes;
      }
    }

    /* Se houver mensagem na sessão, deve ser mostrada */
    if($sessao->has('mensagem_da_pagina_waifu')){
      $valores['waifu']['mensagem'] = $sessao->get('mensagem_da_pagina_waifu');
      $sessao->forget('mensagem_da_pagina_waifu');
      $sessao->save();
    }

    return Inertia::render('waifu/waifu', $valores);
  }

  private function mostrar_inscricoes($pk_waifu){
    $waifu_model = new WaifuModel();

    $valores_deste_metodo = array();

    $requisicao = $this->get_requisicao();

    /* Preparando os filtros */
    $filtros = array();
    $nome_do_torneio = trim($requisicao->get('filtro_nome_do_torneio') ?? '');
    if($nome_do_torneio !== ''){
      $filtros['nome_do_torneio'] = $nome_do_torneio;
    }
    $valores_deste_metodo['filtro_nome_do_torneio'] = $nome_do_torneio;

    $tipo_do_torneio = trim($requisicao->get('filtro_tipo_do_torneio') ?? '');
    if($tipo_do_torneio !== ''){
      $filtros['tipo_do_torneio'] = $tipo_do_torneio;
    }
    $valores_deste_metodo['filtro_tipo_do_torneio'] = $tipo_do_torneio;

    $status_do_torneio = trim($requisicao->get('filtro_status_do_torneio') ?? '');
    if($status_do_torneio !== ''){
      $filtros['status_do_torneio'] = $status_do_torneio;
    }
    $valores_deste_metodo['filtro_status_do_torneio'] = $status_do_torneio;

    /* Preparando a ordenação */
    $ordenacao = $requisicao->get('ordenacao');
    switch($ordenacao){
      case 'padrao':
      case 'nome_a_z':
      case 'nome_z_a':
      case 'tipo_crescente':
      case 'tipo_decrescente':
      case 'status_crescente':
      case 'status_decrescente':
      case 'etapa_crescente':
      case 'etapa_decrescente':
        break;
      default:
        $ordenacao = 'padrao';
        break;
    }
    $valores_deste_metodo['ordenacao'] = $ordenacao;

    /* Preparando a paginação */
    $quantidade_por_pagina = self::QUANTIDADE_PADRAO_POR_PAGINA;

    $pagina = (int) $requisicao->get('pagina');
    if($pagina < 1){
      $pagina = 1;
    }

    $quantidade_de_paginas = $this->calcular_quantidade_de_paginas_das_inscricoes($pk_waifu, $filtros, $quantidade_por_pagina);
    if($pagina > $quantidade_de_paginas){
      $pagina = $quantidade_de_paginas;
    }

    $valores_deste_metodo['pagina_atual'] = $pagina;
    $valores_deste_metodo['ultima_pagina'] = $quantidade_de_paginas;

    $descartar = $quantidade_por_pagina * $pagina - $quantidade_por_pagina;
    $descartar = max($descartar, 0);

    /* Selecionando as inscrições */
    $inscricoes = $waifu_model->selecionar_inscricoes($pk_waifu, $filtros, $ordenacao, $quantidade_por_pagina, $descartar);
    $array_inscricoes = array();
    foreach($inscricoes as $inscricao){
      $array_inscricao = array();

      $array_inscricao['id_do_torneio'] = $inscricao->get_torneio()->get_pk_torneio();

      $nome = $inscricao->get_torneio()->get_nome();
      $array_inscricao['nome_do_torneio'] = $nome;

      $quantidade_de_waifus = $inscricao->get_torneio()->get_quantidade_de_waifus();
      $array_enum_quantidade_de_waifus = $inscricao->get_torneio()->enum_quantidade_de_waifus();
      $tipo_do_torneio = $array_enum_quantidade_de_waifus[$quantidade_de_waifus];
      $array_inscricao['tipo_do_torneio'] = $tipo_do_torneio;

      $status = $inscricao->get_torneio()->get_status();
      $array_enum_status = $inscricao->get_torneio()->enum_status();
      $status_frase = $array_enum_status[$status];
      $array_inscricao['status_do_torneio'] = $status_frase;

      $etapa_em_forma_de_texto = '';
      if($status === 'aguardando_inscricoes'){
        $etapa_em_forma_de_texto = 'Inscrita';
      }else{
        $etapa = $inscricao->get_etapa();
        $etapa_maxima_do_torneio = log($quantidade_de_waifus, 2) + 1;
        switch($etapa_maxima_do_torneio - $etapa){
          case 0:
            $etapa_em_forma_de_texto = 'Vitória';
            break;
          case 1:
            $etapa_em_forma_de_texto = 'Final';
            break;
          case 2:
            $etapa_em_forma_de_texto = 'Semifinal';
            break;
          case 3:
            $etapa_em_forma_de_texto = 'Quartas de final';
            break;
          case 4:
            $etapa_em_forma_de_texto = 'Oitavas de final';
            break;
          case 5:
            $etapa_em_forma_de_texto = 'Dezesseis avos de final';
            break;
        }
      }
      $array_inscricao['etapa_que_atingiu_a_waifu_no_torneio'] = $etapa_em_forma_de_texto;

      $array_inscricoes[] = $array_inscricao;
    }
    $valores_deste_metodo['lista'] = $array_inscricoes;

    return $valores_deste_metodo;
  }

  public function mostrar_inscricoes_ajax(){
    $waifu_model = new WaifuModel();

    $retorno = array();

    /* Validando o ID da waifu informado na URL */
    $requisicao = $this->get_requisicao();
    $pk_waifu = $requisicao->get('id');
    if(!is_numeric($pk_waifu) or $pk_waifu <= 0 or floor($pk_waifu) != $pk_waifu){
      $mensagem = 'ID inválido, o ID da waifu precisa ser um número natural maior que zero.';
      $retorno['mensagem'] = $mensagem;
      echo json_encode($retorno);
      die;
    }else{
      $array_resultado = $waifu_model->selecionar_waifu($pk_waifu);
      if(isset($array_resultado['mensagem_do_model'])){
        $retorno['mensagem'] = $array_resultado['mensagem_do_model'];
        echo json_encode($retorno);
        die;
      }
    }

    $retorno = $this->mostrar_inscricoes($pk_waifu);
    echo json_encode($retorno);
  }

  private function calcular_quantidade_de_paginas_das_inscricoes($pk_waifu, $filtros, $quantidade_por_pagina){
    $waifu_model = new WaifuModel();

    $array_resultado = $waifu_model->contar_inscricoes($pk_waifu, $filtros);
    $quantidade_de_paginas = ceil($array_resultado['quantidade'] / $quantidade_por_pagina);

    return $quantidade_de_paginas;
  }

}
