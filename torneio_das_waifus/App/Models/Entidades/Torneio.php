<?php

namespace App\Models\Entidades;

final class Torneio{
  private $pk_torneio;
  private $fk_vencedora;
  private $nome;
  private $momento_do_inicio;
  private $quantidade_de_waifus;
  private $etapa;
  private $status;
  private $quantidade_de_waifus_inscritas;
  private $waifu_vencedora;

  public function __construct($array_torneio = array()){
    if(isset($array_torneio['pk_torneio'])){
      $this->pk_torneio = $array_torneio['pk_torneio'];
    }
    if(isset($array_torneio['fk_vencedora'])){
      $this->fk_vencedora = $array_torneio['fk_vencedora'];
    }
    if(isset($array_torneio['nome'])){
      $this->nome = $array_torneio['nome'];
    }
    if(isset($array_torneio['momento_do_inicio'])){
      $this->momento_do_inicio = $array_torneio['momento_do_inicio'];
    }
    if(isset($array_torneio['quantidade_de_waifus'])){
      $this->quantidade_de_waifus = $array_torneio['quantidade_de_waifus'];
    }
    if(isset($array_torneio['etapa'])){
      $this->etapa = $array_torneio['etapa'];
    }
    if(isset($array_torneio['status'])){
      $this->status = $array_torneio['status'];
    }
    if(isset($array_torneio['quantidade_de_waifus_inscritas'])){
      $this->quantidade_de_waifus_inscritas = $array_torneio['quantidade_de_waifus_inscritas'];
    }
    if(isset($array_torneio['waifu_vencedora'])){
      $this->waifu_vencedora = $array_torneio['waifu_vencedora'];
    }
  }

  public function set_pk_torneio($pk_torneio){
    $this->pk_torneio = $pk_torneio;
  }

  public function set_fk_vencedora($fk_vencedora){
    $this->fk_vencedora = $fk_vencedora;
  }

  public function set_nome($nome){
    $this->nome = $nome;
  }

  public function set_momento_do_inicio($momento_do_inicio){
    $this->momento_do_inicio = $momento_do_inicio;
  }

  public function set_quantidade_de_waifus($quantidade_de_waifus){
    $this->quantidade_de_waifus = $quantidade_de_waifus;
  }

  public function set_etapa($etapa){
    $this->etapa = $etapa;
  }

  public function set_status($status){
    $this->status = $status;
  }

  public function set_quantidade_de_waifus_inscritas($quantidade_de_waifus_inscritas){
    $this->quantidade_de_waifus_inscritas = $quantidade_de_waifus_inscritas;
  }

  public function set_waifu_vencedora($waifu_vencedora){
    $this->waifu_vencedora = $waifu_vencedora;
  }

  public function get_pk_torneio(){
    return $this->pk_torneio;
  }

  public function get_fk_vencedora(){
    return $this->fk_vencedora;
  }

  public function get_nome(){
    return $this->nome;
  }

  public function get_momento_do_inicio(){
    return $this->momento_do_inicio;
  }

  public function get_quantidade_de_waifus(){
    return $this->quantidade_de_waifus;
  }

  public function get_etapa(){
    return $this->etapa;
  }

  public function get_status(){
    return $this->status;
  }

  public function get_quantidade_de_waifus_inscritas(){
    return $this->quantidade_de_waifus_inscritas;
  }

  public function get_waifu_vencedora(){
    return $this->waifu_vencedora;
  }

  public function quantidade_minima_de_caracteres($atributo){
    switch($atributo){
      case 'nome':
        return 2;
    }
    return -1;
  }

  // O método abaixo deve ser sempre igual ou mais restritivo que o banco de dados
  public function quantidade_maxima_de_caracteres($atributo){
    switch($atributo){
      case 'nome':
        return 120;
    }
    return -1;
  }

  public function enum_quantidade_de_waifus(){
    $array_enum['2'] = '2 Waifus';
    $array_enum['4'] = '4 Waifus';
    $array_enum['8'] = '8 Waifus';
    $array_enum['16'] = '16 Waifus';
    $array_enum['32'] = '32 Waifus';
    return $array_enum;
  }

  public function enum_status(){
    $array_enum['aguardando_inscricoes'] = 'Aguardando inscrições';
    $array_enum['torneio_iniciado'] = 'Já iniciado';
    $array_enum['torneio_encerrado'] = 'Encerrado';
    return $array_enum;
  }

}
