<?php

namespace App\Http\Controllers;

use App\Models\WaifusModel;
use Inertia\Inertia;

final class WaifusController extends TemplateLayoutController{
  private const QUANTIDADE_PADRAO_POR_PAGINA = 16;

  public function carregar_pagina(){
    $valores = $this->valores_do_template_layout();

    $valores['waifus']['waifus'] = $this->mostrar_waifus();

    return Inertia::render('waifus/waifus', $valores);
  }

  private function mostrar_waifus(){
    $waifus_model = new WaifusModel();

    $valores_deste_metodo = array();

    $requisicao = $this->get_requisicao();

    /* Preparando os filtros */
    $filtros = array();
    $nome_da_waifu = trim($requisicao->get('filtro_nome_da_waifu') ?? '');
    if($nome_da_waifu !== ''){
      $filtros['nome_da_waifu'] = $nome_da_waifu;
    }
    $valores_deste_metodo['filtro_nome'] = $nome_da_waifu;

    /* Preparando a ordenação */
    $ordenacao = $requisicao->get('ordenacao');
    switch($ordenacao){
      case 'padrao':
      case 'nome_a_z':
      case 'nome_z_a':
        break;
      default:
        $ordenacao = 'padrao';
        break;
    }
    $valores_deste_metodo['ordenacao'] = $ordenacao;

    /* Preparando a paginação */
    $quantidade_por_pagina = self::QUANTIDADE_PADRAO_POR_PAGINA;

    $pagina = (int) $requisicao->get('pagina');
    if($pagina < 1){
      $pagina = 1;
    }
    $quantidade_de_paginas = $this->calcular_quantidade_de_paginas_das_waifus($filtros, $quantidade_por_pagina);
    if($pagina > $quantidade_de_paginas){
      $pagina = $quantidade_de_paginas;
    }

    $valores_deste_metodo['pagina_atual'] = $pagina;
    $valores_deste_metodo['ultima_pagina'] = $quantidade_de_paginas;

    $descartar = $quantidade_por_pagina * $pagina - $quantidade_por_pagina;
    $descartar = max($descartar, 0);

    /* Preparando o resultado */
    $waifus = $waifus_model->selecionar_waifus($filtros, $ordenacao, $quantidade_por_pagina, $descartar);
    $array_waifus = array();

    foreach($waifus as $waifu){
      $array_waifu = array();

      $id = $waifu->get_pk_waifu();
      $array_waifu['id'] = $id;

      $imagem = $waifu->get_imagem();
      $array_waifu['imagem'] = $imagem;

      $nome = $waifu->get_nome();
      $array_waifu['nome'] = $nome;

      $array_waifus[] = $array_waifu;
    }
    $valores_deste_metodo['lista'] = $array_waifus;

    return $valores_deste_metodo;
  }

  public function mostrar_waifus_ajax(){
    $retorno = $this->mostrar_waifus();
    echo json_encode($retorno);
  }

  private function calcular_quantidade_de_paginas_das_waifus($filtros, $quantidade_por_pagina){
    $waifus_model = new WaifusModel();

    $array_resultado = $waifus_model->contar_waifus($filtros);
    $quantidade_de_paginas = ceil($array_resultado['quantidade'] / $quantidade_por_pagina);

    return $quantidade_de_paginas;
  }

}
