<?php

namespace App\Models\Entidades;

final class InscricaoDaWaifu{
  private $pk_inscricao_da_waifu;
  private $fk_torneio;
  private $fk_waifu;
  private $etapa;
  private $posicao_inicial;
  private $torneio;
  private $waifu;

  public function __construct($array_inscricao_da_waifu = array()){
    if(isset($array_inscricao_da_waifu['pk_inscricao_da_waifu'])){
      $this->pk_inscricao_da_waifu = $array_inscricao_da_waifu['pk_inscricao_da_waifu'];
    }
    if(isset($array_inscricao_da_waifu['fk_torneio'])){
      $this->fk_torneio = $array_inscricao_da_waifu['fk_torneio'];
    }
    if(isset($array_inscricao_da_waifu['fk_waifu'])){
      $this->fk_waifu = $array_inscricao_da_waifu['fk_waifu'];
    }
    if(isset($array_inscricao_da_waifu['etapa'])){
      $this->etapa = $array_inscricao_da_waifu['etapa'];
    }
    if(isset($array_inscricao_da_waifu['posicao_inicial'])){
      $this->posicao_inicial = $array_inscricao_da_waifu['posicao_inicial'];
    }
    if(isset($array_inscricao_da_waifu['torneio'])){
      $this->torneio = $array_inscricao_da_waifu['torneio'];
    }
    if(isset($array_inscricao_da_waifu['waifu'])){
      $this->waifu = $array_inscricao_da_waifu['waifu'];
    }
  }

  public function set_pk_inscricao_da_waifu($pk_inscricao_da_waifu){
    $this->pk_inscricao_da_waifu = $pk_inscricao_da_waifu;
  }

  public function set_fk_torneio($fk_torneio){
    $this->fk_torneio = $fk_torneio;
  }

  public function set_fk_waifu($fk_waifu){
    $this->fk_waifu = $fk_waifu;
  }

  public function set_etapa($etapa){
    $this->etapa = $etapa;
  }

  public function set_posicao_inicial($posicao_inicial){
    $this->posicao_inicial = $posicao_inicial;
  }

  public function set_torneio($torneio){
    $this->torneio = $torneio;
  }

  public function set_waifu($waifu){
    $this->waifu = $waifu;
  }

  public function get_pk_inscricao_da_waifu(){
    return $this->pk_inscricao_da_waifu;
  }

  public function get_fk_torneio(){
    return $this->fk_torneio;
  }

  public function get_fk_waifu(){
    return $this->fk_waifu;
  }

  public function get_etapa(){
    return $this->etapa;
  }

  public function get_posicao_inicial(){
    return $this->posicao_inicial;
  }

  public function get_torneio(){
    return $this->torneio;
  }

  public function get_waifu(){
    return $this->waifu;
  }

}
