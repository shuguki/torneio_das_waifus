<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Exception;

final class NovaWaifuModel{

  public function verifica_disponibilidade_de_nome_da_waifu($nome){
    $query = DB::table('waifu');
    $query = $query->addSelect('nome');
    $query = $query->where('nome', '=', $nome);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    if(count($array_resultado) > 0){
      $mensagem_do_model = 'O nome escolhido já é o nome de uma waifu cadastrada anteriormente.';
      $mensagem_do_model .= ' Por favor, utilize outro nome para cadastrar a waifu desejada.';
      $array_resultado['mensagem_do_model'] = $mensagem_do_model;
    }

    return $array_resultado;
  }

  public function cadastrar_waifu($waifu){
    $insert['nome'] = $waifu->get_nome();
    $insert['imagem'] = $waifu->get_imagem();

    $array_resultado = array();
    try{
      DB::table('waifu')->insert($insert);
    }catch(Exception $excecao){
      $codigo_da_excecao = $excecao->getCode();
      switch($codigo_da_excecao){
        case 1062:
          $mensagem_do_model = 'Já existe uma waifu cadastrada com uma ou mais destas informações.';
          $array_resultado['mensagem_do_model'] = $mensagem_do_model;
          break;
        default:
          $array_resultado['mensagem_do_model'] = $excecao->getMessage();
          break;
      }
    }

    return $array_resultado;
  }

}
