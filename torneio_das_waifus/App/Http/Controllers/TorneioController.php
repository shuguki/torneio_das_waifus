<?php

namespace App\Http\Controllers;

use App\Models\TorneioModel;
use Inertia\Inertia;
use DateTime;
use DateTimeZone;

final class TorneioController extends TemplateLayoutController{
  private const PONTUACAO_MAXIMA = 15;

  public function carregar_pagina($redirecionar_com_id = false){
    if($redirecionar_com_id !== false){
      //Redireciona para si mesmo, motivo: limpar a requisição.
      header("Location: /torneio?id=$redirecionar_com_id");
      die;
    }

    $valores = $this->valores_do_template_layout();
    $sessao = session();

    /* Colocando valores iniciais nas variáveis para não ficarem undefined no Vue */
    $valores['torneio']['mensagem'] = '';
    $valores['torneio']['tipo_de_mensagem'] = 'sucesso';
    $valores['torneio']['id_valido'] = true;
    $valores['torneio']['id'] = '';
    $valores['torneio']['nome'] = '';
    $valores['torneio']['status'] = '';
    $valores['torneio']['status_frase'] = '';
    $valores['torneio']['quantidade_de_waifus'] = 0;
    $valores['torneio']['quantidade_de_waifus_frase'] = '';
    $valores['torneio']['vagas_disponiveis'] = 0;
    $valores['torneio']['momento_do_inicio'] = '';
    $valores['torneio']['etapa'] = 0;
    $valores['torneio']['etapa_frase'] = '';
    $valores['torneio']['waifu_vencedora'] = array();
    $valores['torneio']['waifu_vencedora']['id'] = '';
    $valores['torneio']['waifu_vencedora']['nome'] = '';
    $valores['torneio']['waifu_vencedora']['imagem'] = '';
    $valores['torneio']['pontuacoes'] = array();

    $torneio_model = new TorneioModel();

    /* Validando o ID do torneio informado na URL */
    $requisicao = $this->get_requisicao();
    $pk_torneio = $requisicao->get('id');
    if(!is_numeric($pk_torneio) or $pk_torneio <= 0 or floor($pk_torneio) != $pk_torneio){
      $mensagem = 'ID inválido, o ID do torneio precisa ser um número natural maior que zero.';
      $valores['torneio']['mensagem'] = $mensagem;
      $valores['torneio']['tipo_de_mensagem'] = 'falha';
      $valores['torneio']['id_valido'] = false;
    }else{
      /* Consultando e mostrando informações do torneio */
      $array_resultado = $torneio_model->selecionar_torneio($pk_torneio);
      if(isset($array_resultado['mensagem_do_model'])){
        $valores['torneio']['mensagem'] = $array_resultado['mensagem_do_model'];
        $valores['torneio']['tipo_de_mensagem'] = 'falha';
        $valores['torneio']['id_valido'] = false;
      }else{
        $torneio = $array_resultado[0];
        $valores['torneio']['id'] = $torneio->get_pk_torneio();
        $valores['torneio']['nome'] = $torneio->get_nome();

        $status = $torneio->get_status();
        $valores['torneio']['status'] = $status;

        $array_enum_status = $torneio->enum_status();
        $status_frase = $array_enum_status[$status];
        $valores['torneio']['status_frase'] = $status_frase;

        $quantidade_de_waifus = $torneio->get_quantidade_de_waifus();
        $valores['torneio']['quantidade_de_waifus'] = $quantidade_de_waifus;

        $array_enum_quantidade_de_waifus = $torneio->enum_quantidade_de_waifus();
        $quantidade_de_waifus_frase = $array_enum_quantidade_de_waifus[$quantidade_de_waifus];
        $valores['torneio']['quantidade_de_waifus_frase'] = $quantidade_de_waifus_frase;

        $quantidade_de_vagas_disponiveis = $quantidade_de_waifus - $torneio->get_quantidade_de_waifus_inscritas();
        if($quantidade_de_vagas_disponiveis < 0){
          $quantidade_de_vagas_disponiveis = 0;
        }
        $valores['torneio']['vagas_disponiveis'] = $quantidade_de_vagas_disponiveis;

        $momento_do_inicio = $torneio->get_momento_do_inicio();
        if($momento_do_inicio !== null){
          $sem_fuso_horario = new DateTimeZone('GMT');
          $objeto_date_time = new DateTime($momento_do_inicio, $sem_fuso_horario);

          $fuso_horario_de_brasilia = new DateTimeZone('-0300');
          $objeto_date_time->setTimeZone($fuso_horario_de_brasilia);

          $momento_do_inicio = $objeto_date_time->format('Y-m-d H:i:s');

          $momento_do_inicio = $this->converter_para_horario_data_do_html($momento_do_inicio);
          $valores['torneio']['momento_do_inicio'] = $momento_do_inicio;
        }

        $etapa_atual = $torneio->get_etapa();
        $valores['torneio']['etapa'] = $etapa_atual;
        if($status === 'torneio_iniciado'){
          $etapa_maxima_do_torneio = log($quantidade_de_waifus, 2) + 1;
          switch($etapa_maxima_do_torneio - $etapa_atual){
            case 1:
              $valores['torneio']['etapa_frase'] = 'final';
              break;
            case 2:
              $valores['torneio']['etapa_frase'] = 'semifinal';
              break;
            case 3:
              $valores['torneio']['etapa_frase'] = 'quartas de final';
              break;
            case 4:
              $valores['torneio']['etapa_frase'] = 'oitavas de final';
              break;
            case 5:
              $valores['torneio']['etapa_frase'] = 'dezesseis avos de final';
              break;
          }
        }

        $waifu_vencedora = $torneio->get_waifu_vencedora();
        if($waifu_vencedora !== null){
          $id_da_waifu_que_venceu = $waifu_vencedora->get_pk_waifu();
          $valores['torneio']['waifu_vencedora']['id'] = $id_da_waifu_que_venceu;

          $nome_da_waifu_que_venceu = $waifu_vencedora->get_nome();
          $valores['torneio']['waifu_vencedora']['nome'] = $nome_da_waifu_que_venceu;

          $imagem_da_waifu_que_venceu = $waifu_vencedora->get_imagem();
          $valores['torneio']['waifu_vencedora']['imagem'] = $imagem_da_waifu_que_venceu;
        }

        $array_resultado_inscricoes = $torneio_model->selecionar_inscricoes_do_torneio($pk_torneio);
        foreach($array_resultado_inscricoes as $inscricao){
          $pk_inscricao = $inscricao->get_pk_inscricao_da_waifu();
          $id_da_waifu = $inscricao->get_waifu()->get_pk_waifu();
          $nome_da_waifu = $inscricao->get_waifu()->get_nome();
          $imagem_da_waifu = $inscricao->get_waifu()->get_imagem();
          $etapa_da_inscricao = $inscricao->get_etapa();

          $posicao = $inscricao->get_posicao_inicial() * 2; //Começa sendo o dobro, depois divide por 2.
          $array_resultado_pontuacoes = $torneio_model->selecionar_pontuacoes_da_inscricao_no_torneio($pk_inscricao);
          for($etapa = 1; $etapa <= $etapa_da_inscricao; $etapa++){
            if($status === 'torneio_encerrado' && $etapa === $etapa_atual){
              //Última etapa de um torneio encerrado não possui pontuação.
              break;
            }

            $posicao /= 2;
            $posicao = (int) ceil($posicao);

            $valores['torneio']['pontuacoes'][$etapa - 1][$posicao - 1]['valor_da_pontuacao'] = 0;
            if(isset($array_resultado_pontuacoes['etapa_'.$etapa])){
              $valor_da_pontuacao = $array_resultado_pontuacoes['etapa_'.$etapa];
              $valores['torneio']['pontuacoes'][$etapa - 1][$posicao - 1]['valor_da_pontuacao'] = $valor_da_pontuacao;
            }
            $valores['torneio']['pontuacoes'][$etapa - 1][$posicao - 1]['id_da_inscricao'] = $pk_inscricao;
            $valores['torneio']['pontuacoes'][$etapa - 1][$posicao - 1]['id_da_waifu'] = $id_da_waifu;
            $valores['torneio']['pontuacoes'][$etapa - 1][$posicao - 1]['nome_da_waifu'] = $nome_da_waifu;
            $valores['torneio']['pontuacoes'][$etapa - 1][$posicao - 1]['imagem_da_waifu'] = $imagem_da_waifu;
            $valores['torneio']['pontuacoes'][$etapa - 1][$posicao - 1]['etapa_da_pontuacao'] = $etapa;
          }
        }
      }
    }

    /* Se houver mensagem na sessão, deve ser mostrada */
    if($sessao->has('mensagem_da_pagina_torneio')){
      $valores['torneio']['mensagem'] = $sessao->get('mensagem_da_pagina_torneio');
      $valores['torneio']['tipo_de_mensagem'] = $sessao->get('tipo_da_mensagem_da_pagina_torneio');
      $sessao->forget('mensagem_da_pagina_torneio');
      $sessao->forget('tipo_da_mensagem_da_pagina_torneio');
      $sessao->save();
    }

    return Inertia::render('torneio/torneio', $valores);
  }

  public function votar_na_waifu(){
    $torneio_model = new TorneioModel();

    $sessao = session();

    /* Obtendo valores do formulário */
    $requisicao = $this->get_requisicao();
    $pk_inscricao = $requisicao->post('id_da_inscricao');
    $etapa_da_pontuacao = $requisicao->post('etapa_da_pontuacao');

    /* Validações */
    if(!is_numeric($pk_inscricao) or $pk_inscricao <= 0 or floor($pk_inscricao) != $pk_inscricao){
      $mensagem = 'Seu voto não foi computado.';
      $mensagem .= ' O ID da inscrição precisa ser um número natural maior que zero.';
      $sessao->put('mensagem_da_pagina_torneio', $mensagem);
      $sessao->put('tipo_da_mensagem_da_pagina_torneio', 'falha');
      $sessao->save();
      $this->carregar_pagina(0);
      die;
    }
    $array_resultado = $torneio_model->selecionar_inscricao($pk_inscricao);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = 'Seu voto não foi computado.';
      $mensagem .= " {$array_resultado['mensagem_do_model']}";
      $sessao->put('mensagem_da_pagina_torneio', $mensagem);
      $sessao->put('tipo_da_mensagem_da_pagina_torneio', 'falha');
      $sessao->save();
      $this->carregar_pagina(0);
      die;
    }

    $inscricao = $array_resultado[0];
    $torneio = $inscricao->get_torneio();
    $pk_torneio = $torneio->get_pk_torneio();
    if($torneio->get_status() === 'torneio_encerrado'){
      $mensagem = 'Seu voto não foi computado.';
      $mensagem .= ' Este torneio já está encerrado.';
      $sessao->put('mensagem_da_pagina_torneio', $mensagem);
      $sessao->put('tipo_da_mensagem_da_pagina_torneio', 'falha');
      $sessao->save();
      $this->carregar_pagina($pk_torneio);
      die;
    }
    if(!is_numeric($etapa_da_pontuacao) or $etapa_da_pontuacao <= 0 or floor($etapa_da_pontuacao) != $etapa_da_pontuacao){
      $mensagem = 'Seu voto não foi computado.';
      $mensagem .= ' A etapa do torneio por onde você está tentando votar não é válida.';
      $sessao->put('mensagem_da_pagina_torneio', $mensagem);
      $sessao->put('tipo_da_mensagem_da_pagina_torneio', 'falha');
      $sessao->save();
      $this->carregar_pagina($pk_torneio);
      die;
    }
    if($etapa_da_pontuacao < $torneio->get_etapa()){
      $mensagem = 'Seu voto não foi computado.';
      $mensagem .= ' Esta etapa já terminou, vote por meio da etapa que estiver decorrendo.';
      $sessao->put('mensagem_da_pagina_torneio', $mensagem);
      $sessao->put('tipo_da_mensagem_da_pagina_torneio', 'falha');
      $sessao->save();
      $this->carregar_pagina($pk_torneio);
      die;
    }
    if($etapa_da_pontuacao > $torneio->get_etapa()){
      $mensagem = 'Seu voto não foi computado.';
      $mensagem .= ' Esta etapa ainda não começou, vote por meio da etapa que estiver decorrendo.';
      $sessao->put('mensagem_da_pagina_torneio', $mensagem);
      $sessao->put('tipo_da_mensagem_da_pagina_torneio', 'falha');
      $sessao->save();
      $this->carregar_pagina($pk_torneio);
      die;
    }

    /* Verificando se a inscrição da waifu está mesmo nesta etapa */
    if($inscricao->get_etapa() != $etapa_da_pontuacao){
      $mensagem = 'Seu voto não foi computado.';
      $mensagem .= ' Esta waifu não está na etapa pela qual você está tentando votar.';
      $sessao->put('mensagem_da_pagina_torneio', $mensagem);
      $sessao->put('tipo_da_mensagem_da_pagina_torneio', 'falha');
      $sessao->save();
      $this->carregar_pagina($pk_torneio);
      die;
    }

    $array_resultado = $torneio_model->selecionar_pontuacao_da_waifu($pk_inscricao, $etapa_da_pontuacao);
    $pontuacao_da_waifu = $array_resultado[0];

    /* Verificando se atingiu pontuação máxima */
    if(self::PONTUACAO_MAXIMA > 0){
      if($pontuacao_da_waifu->get_valor() >= self::PONTUACAO_MAXIMA){
        $mensagem = 'Seu voto não foi computado.';
        $mensagem .= ' Esta waifu já possui pontuação máxima e irá para a próxima etapa do torneio.';
        $sessao->put('mensagem_da_pagina_torneio', $mensagem);
        $sessao->put('tipo_da_mensagem_da_pagina_torneio', 'falha');
        $sessao->save();
        $this->carregar_pagina($pk_torneio);
        die;
      }

      /* Verificando pontuação da adversária */
      $posicao_inicial = $inscricao->get_posicao_inicial();
      $possibilidades_de_adversarias = pow(2, $etapa_da_pontuacao - 1);
      $posicao_na_etapa = $posicao_inicial;
      for($i = 1; $i < $etapa_da_pontuacao; $i++){
        $posicao_na_etapa = $posicao_na_etapa / 2;
        $posicao_na_etapa = ceil($posicao_na_etapa);
      }

      if($posicao_na_etapa % 2 === 0){
        $posicao_da_adversaria = $posicao_na_etapa - 1;
      }else{
        $posicao_da_adversaria = $posicao_na_etapa + 1;
      }

      $posicoes_iniciais_das_possiveis_adversarias = array();
      for($i = 1; $i < $etapa_da_pontuacao; $i++){
        $posicao_da_adversaria = $posicao_da_adversaria * 2;
      }
      $posicoes_iniciais_das_possiveis_adversarias[] = $posicao_da_adversaria;
      for($i = 1; $i < $possibilidades_de_adversarias; $i++){
        $posicoes_iniciais_das_possiveis_adversarias[] = --$posicao_da_adversaria;
      }

      $array_resultado = $torneio_model->selecionar_pontuacao_da_adversaria($pk_torneio, $posicoes_iniciais_das_possiveis_adversarias, $etapa_da_pontuacao);
      $pontuacao_da_adversaria = $array_resultado[0];
      if($pontuacao_da_adversaria->get_valor() >= self::PONTUACAO_MAXIMA){
        $mensagem = 'Seu voto não foi computado.';
        $mensagem .= ' Esta waifu já foi derrotada pela adversária.';
        $mensagem .= ' A adversária já atingiu a pontuação máxima e irá para a próxima etapa do torneio.';
        $sessao->put('mensagem_da_pagina_torneio', $mensagem);
        $sessao->put('tipo_da_mensagem_da_pagina_torneio', 'falha');
        $sessao->save();
        $this->carregar_pagina($pk_torneio);
        die;
      }
    }

    /* Votar */
    if($pontuacao_da_waifu->get_pk_pontuacao() === null){
      $pk_pontuacao = $pontuacao_da_waifu->get_pk_pontuacao();
      $array_resultado = $torneio_model->votar_na_waifu_inserindo_pontuacao($pk_inscricao, $etapa_da_pontuacao);
      if(isset($array_resultado['mensagem_do_model'])){
        $mensagem = 'Seu voto não foi computado.';
        $mensagem .= " {$array_resultado['mensagem_do_model']}";
        $sessao->put('mensagem_da_pagina_torneio', $mensagem);
        $sessao->put('tipo_da_mensagem_da_pagina_torneio', 'falha');
        $sessao->save();
        $this->carregar_pagina($pk_torneio);
        die;
      }
    }else{
      $pk_pontuacao = $pontuacao_da_waifu->get_pk_pontuacao();
      $array_resultado = $torneio_model->votar_na_waifu_incrementando_o_valor($pk_pontuacao);
      if(isset($array_resultado['mensagem_do_model'])){
        $mensagem = 'Seu voto não foi computado.';
        $mensagem .= " {$array_resultado['mensagem_do_model']}";
        $sessao->put('mensagem_da_pagina_torneio', $mensagem);
        $sessao->put('tipo_da_mensagem_da_pagina_torneio', 'falha');
        $sessao->save();
        $this->carregar_pagina($pk_torneio);
        die;
      }
    }

    /* Se houver limite de pontuação, pode adiantar o início da próxima etapa */
    if(self::PONTUACAO_MAXIMA > 0){
      $etapa_atual = $torneio->get_etapa();
      $array_resultado = $torneio_model->selecionar_pontuacoes_da_etapa_atual_do_torneio($pk_torneio, $etapa_atual);
      $inscricoes_que_venceram = array();
      $pontuacao_anterior = null;
      $contador_de_pontuacoes = 0;
      foreach($array_resultado as $pontuacao){
        $contador_de_pontuacoes++;
        if($contador_de_pontuacoes % 2 === 0){
          if($pontuacao->get_valor() > $pontuacao_anterior->get_valor()){
            if($pontuacao->get_valor() >= self::PONTUACAO_MAXIMA){
              $inscricoes_que_venceram[] = $pontuacao->get_inscricao();
            }
          }elseif($pontuacao->get_valor() < $pontuacao_anterior->get_valor()){
            if($pontuacao_anterior->get_valor() >= self::PONTUACAO_MAXIMA){
              $inscricoes_que_venceram[] = $pontuacao_anterior->get_inscricao();
            }
          }else{
            if($pontuacao->get_valor() >= self::PONTUACAO_MAXIMA){
              $sorteio = random_int(1, 2);
              if($sorteio === 1){
                $inscricoes_que_venceram[] = $pontuacao_anterior->get_inscricao();
              }elseif($sorteio === 2){
                $inscricoes_que_venceram[] = $pontuacao->get_inscricao();
              }
            }
          }
        }else{
          $pontuacao_anterior = $pontuacao;
        }
      }

      /* Atualizar o torneio se esta etapa tiver terminado */
      $metade = $torneio->get_quantidade_de_waifus() / pow(2, $etapa_atual);
      $quantidade_de_vencedoras = count($inscricoes_que_venceram);
      if($quantidade_de_vencedoras === $metade){
        if($metade === 1){
          $pk_waifu_vencedora = $inscricoes_que_venceram[0]->get_fk_waifu();
          $torneio_model->atualizar_torneio_para_proxima_etapa($pk_torneio, $pk_waifu_vencedora);
        }elseif($metade > 1){
          $torneio_model->atualizar_torneio_para_proxima_etapa($pk_torneio);
        }

        /* Atualizar as inscrições que passaram para próxima etapa */
        foreach($inscricoes_que_venceram as $inscricao){
          $torneio_model->atualizar_inscricao_para_proxima_etapa($inscricao);
        }
      }
    }

    $mensagem = 'Seu voto foi computado com sucesso.';
    $sessao->put('mensagem_da_pagina_torneio', $mensagem);
    $sessao->put('tipo_da_mensagem_da_pagina_torneio', 'sucesso');
    $sessao->save();
    $this->carregar_pagina($pk_torneio);
    die;
  }

}
