<?php

namespace App\Http\Controllers;

use App\Models\NovoTorneioModel;
use App\Models\Entidades\Torneio;
use App\Models\Entidades\InscricaoDaWaifu;
use Inertia\Inertia;

final class NovoTorneioController extends TemplateLayoutController{

  public function carregar_pagina($redirecionar = false){
    if($redirecionar){
      //Redireciona para si mesmo, motivo: limpar a requisição.
      header('Location: /novo_torneio');
      die;
    }

    $valores = $this->valores_do_template_layout();
    $sessao = session();

    /* Colocando valores iniciais nas variáveis para não ficarem undefined no Vue */
    $valores['novo_torneio']['mensagem'] = '';
    $valores['novo_torneio']['tipo_de_mensagem'] = 'sucesso';
    $torneio = new Torneio();
    $valores['novo_torneio']['tipos_de_torneios'] = $torneio->enum_quantidade_de_waifus();
    $valores['novo_torneio']['nomes_das_waifus'] = array();

    /* Recolocando valores preenchidos previamente pelo usuário no formulário */
    if($sessao->has('backup_do_formulario_da_pagina_novo_torneio')){
      $backup = $sessao->get('backup_do_formulario_da_pagina_novo_torneio');

      $valores['novo_torneio']['nome'] = $backup['nome'];

      if(isset($torneio->enum_quantidade_de_waifus()[$backup['tipo']])){
        $valores['novo_torneio']['tipo'] = $backup['tipo'];
      }else{
        $valores['novo_torneio']['tipo'] = '0';
      }

      foreach($backup['nomes_das_waifus'] as $nome_da_waifu){
        $valores['novo_torneio']['nomes_das_waifus'][] = $nome_da_waifu;
      }

      $valores['novo_torneio']['tipo_de_mensagem'] = 'falha';
      $sessao->forget('backup_do_formulario_da_pagina_novo_torneio');
      $sessao->save();
    }else{
      $valores['novo_torneio']['nome'] = '';
      $valores['novo_torneio']['tipo'] = '0';
    }

    /* Se houver mensagem na sessão, deve ser mostrada */
    if($sessao->has('mensagem_da_pagina_novo_torneio')){
      $mensagem = $sessao->get('mensagem_da_pagina_novo_torneio');
      $valores['novo_torneio']['mensagem'] = $mensagem;
      $sessao->forget('mensagem_da_pagina_novo_torneio');
      $sessao->save();
    }

    return Inertia::render('novo_torneio/novo_torneio', $valores);
  }

  public function buscar_pelo_nome_da_waifu_ajax(){
    $novo_torneio_model = new NovoTorneioModel();

    $retorno = array();

    $requisicao = $this->get_requisicao();
    $nome_da_waifu = trim($requisicao->post('nome_da_waifu') ?? '');

    if(mb_strlen($nome_da_waifu) > 1){
      $array_resultado = $novo_torneio_model->selecionar_nomes_das_waifus($nome_da_waifu);
      if(isset($array_resultado['mensagem_do_model'])){
        $retorno['mensagem'] = $array_resultado['mensagem_do_model'];
        echo json_encode($retorno);
        die;
      }else{
        $retorno['lista'] = $array_resultado;
      }
    }else{
      $retorno['lista'] = array();
    }

    echo json_encode($retorno);
  }

  public function cadastrar_torneio(){
    $sessao = session();

    $novo_torneio_model = new NovoTorneioModel();

    $torneio = new Torneio();

    /* Obtendo valores do formulário */
    $requisicao = $this->get_requisicao();
    $nome = trim($requisicao->post('nome') ?? '');
    $tipo = $requisicao->post('tipo');
    $nomes_das_waifus = array();
    for($i = 0; $i < 32; $i++){
      $nome_da_waifu = trim($requisicao->post('nome_da_waifu_'.$i) ?? '');
      if($nome_da_waifu === ''){
        continue;
      }
      $nomes_das_waifus[] = $nome_da_waifu;
    }

    while(strpos($nome, '  ') !== false){
      $nome = str_replace('  ', ' ', $nome);
    }

    $backup_do_formulario['nome'] = $nome;
    $backup_do_formulario['tipo'] = $tipo;
    $backup_do_formulario['nomes_das_waifus'] = $nomes_das_waifus;
    $sessao->put('backup_do_formulario_da_pagina_novo_torneio', $backup_do_formulario);
    $sessao->save();

    /* Validações */
    if($nome === ''){
      $mensagem = 'O torneio não foi cadastrado.';
      $mensagem .= ' O campo nome do torneio precisa ser preenchido.';
      $sessao->put('mensagem_da_pagina_novo_torneio', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    $array_resultado = $novo_torneio_model->verifica_disponibilidade_de_nome_do_torneio($nome);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = 'O torneio não foi cadastrado.';
      $mensagem .= ' '.$array_resultado['mensagem_do_model'];
      $sessao->put('mensagem_da_pagina_novo_torneio', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    $minimo = $torneio->quantidade_minima_de_caracteres('nome');
    $maximo = $torneio->quantidade_maxima_de_caracteres('nome');
    $quantidade = mb_strlen($nome);
    if($quantidade < $minimo){
      $mensagem = 'O torneio não foi cadastrado.';
      $mensagem .= " O campo nome do torneio precisa ter no mínimo $minimo caracteres.";
      $sessao->put('mensagem_da_pagina_novo_torneio', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    if($quantidade > $maximo){
      $mensagem = 'O torneio não foi cadastrado.';
      $mensagem .= " O campo nome do torneio não pode ultrapassar $maximo caracteres.";
      $sessao->put('mensagem_da_pagina_novo_torneio', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }

    if($tipo === null or $tipo === ''){
      $mensagem = 'O torneio não foi cadastrado.';
      $mensagem .= ' O tipo do torneio precisa ser escolhido.';
      $sessao->put('mensagem_da_pagina_novo_torneio', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    $quantidade_de_waifus = $tipo;
    if(!isset($torneio->enum_quantidade_de_waifus()[$quantidade_de_waifus])){
      $mensagem = 'O torneio não foi cadastrado.';
      $mensagem .= ' O tipo escolhido para o novo torneio não é um tipo válido.';
      $sessao->put('mensagem_da_pagina_novo_torneio', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }

    /* Cadastrar torneio no banco de dados */
    $torneio->set_nome($nome);
    $torneio->set_quantidade_de_waifus($quantidade_de_waifus);
    $array_resultado = $novo_torneio_model->cadastrar_torneio($torneio);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = 'O torneio não foi cadastrado.';
      $mensagem .= ' '.$array_resultado['mensagem_do_model'];
      $sessao->put('mensagem_da_pagina_novo_torneio', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }else{
      $pk_torneio = $array_resultado['pk_torneio'];

      $array_posicoes_disponiveis = array();
      for($posicao = 1; $posicao <= $quantidade_de_waifus; $posicao++){
        $array_posicoes_disponiveis[] = $posicao;
      }

      for($i = 0; $i <= $quantidade_de_waifus; $i++){
        if(!isset($nomes_das_waifus[$i])){
          break;
        }

        $array_resultado = $novo_torneio_model->selecionar_waifu_pelo_nome($nomes_das_waifus[$i]);
        if(isset($array_resultado['mensagem_do_model'])){
          continue;
        }
        $waifu = $array_resultado[0];
        $pk_waifu = $waifu->get_pk_waifu();

        $array_resultado = $novo_torneio_model->verificar_se_a_waifu_ja_estava_inscrita($pk_waifu, $pk_torneio);
        if($array_resultado['esta_inscrita']){
          continue;
        }

        $inscricao_da_waifu = new InscricaoDaWaifu();
        $inscricao_da_waifu->set_fk_waifu($pk_waifu);
        $inscricao_da_waifu->set_fk_torneio($pk_torneio);
        $posicao_sorteada = $array_posicoes_disponiveis[array_rand($array_posicoes_disponiveis)];
        $inscricao_da_waifu->set_posicao_inicial($posicao_sorteada);
        $array_resultado = $novo_torneio_model->realizar_inscricao($inscricao_da_waifu);
        if(isset($array_resultado['mensagem_do_model'])){
          continue;
        }

        $chave_da_posicao_ocupada = $posicao_sorteada - 1;
        unset($array_posicoes_disponiveis[$chave_da_posicao_ocupada]);
      }

      $mensagem = 'O torneio foi cadastrado com sucesso';

      $array_resultado = $novo_torneio_model->contar_inscricoes_do_torneio($pk_torneio);
      if($array_resultado['quantidade'] >= $quantidade_de_waifus){
        $array_resultado = $novo_torneio_model->iniciar_torneio($pk_torneio);
        if(isset($array_resultado['mensagem_do_model'])){
          $mensagem .= ', porém não foi possível iniciar o torneio. Devido ao erro:';
          $mensagem .= " {$array_resultado['mensagem_do_model']}.";
          $mensagem .= ' Contate um administrador do sistema para que o torneio possa iniciar';
        }else{
          $array_resultado = $novo_torneio_model->atualizar_inscricoes_do_torneio($pk_torneio);
          if(isset($array_resultado['mensagem_do_model'])){
            $mensagem .= ', porém o seguinte erro ocorreu:';
            $mensagem .= " {$array_resultado['mensagem_do_model']}.";
            $mensagem .= ' Reporte o erro para um administrador do sistema';
          }
        }
      }
      $mensagem .= '.';

      $sessao->put('mensagem_da_pagina_novo_torneio', $mensagem);
      $sessao->forget('backup_do_formulario_da_pagina_novo_torneio');
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
  }

}
