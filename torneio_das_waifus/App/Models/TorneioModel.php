<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use App\Models\Entidades\Torneio;
use App\Models\Entidades\Waifu;
use App\Models\Entidades\InscricaoDaWaifu;
use App\Models\Entidades\Pontuacao;
use Exception;

final class TorneioModel{

  public function selecionar_torneio($pk_torneio){
    $query = DB::table('torneio');
    $query = $query->select(
      'pk_torneio',
      'torneio.fk_vencedora',
      'torneio.nome AS nome_do_torneio',
      'torneio.momento_do_inicio',
      'torneio.quantidade_de_waifus',
      'torneio.status',
      'torneio.etapa',
      'waifu.nome AS nome_da_waifu',
      'waifu.imagem AS imagem_da_waifu',
      DB::raw('(SELECT COUNT(*) FROM inscricao_da_waifu WHERE fk_torneio = pk_torneio) AS quantidade_de_waifus_inscritas')
    );
    $query = $query->leftJoin('waifu', 'fk_vencedora', '=', 'pk_waifu'); //LEFT JOIN
    $query = $query->where('pk_torneio', '=', $pk_torneio);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    if(count($array_resultado) === 0){
      $mensagem_do_model = "Nenhum torneio com ID $pk_torneio foi encontrado no banco de dados";
      $mensagem_do_model .= ' do sistema.';
      $array_resultado['mensagem_do_model'] = $mensagem_do_model;
    }else{
      $array_valores_do_banco_de_dados = (array) $array_resultado[0];
      $torneio = new Torneio($array_valores_do_banco_de_dados);
      $torneio->set_nome($array_valores_do_banco_de_dados['nome_do_torneio']);

      $waifu = new Waifu();
      $waifu->set_pk_waifu($array_valores_do_banco_de_dados['fk_vencedora']);
      $waifu->set_nome($array_valores_do_banco_de_dados['nome_da_waifu']);
      $waifu->set_imagem($array_valores_do_banco_de_dados['imagem_da_waifu']);

      $torneio->set_waifu_vencedora($waifu);

      $array_melhorado[] = $torneio;

      $array_resultado = $array_melhorado;
    }

    return $array_resultado;
  }

  public function selecionar_inscricoes_do_torneio($pk_torneio){
    $query = DB::table('inscricao_da_waifu');
    $query = $query->addSelect('pk_inscricao_da_waifu');
    $query = $query->addSelect('inscricao_da_waifu.fk_torneio AS pk_torneio');
    $query = $query->addSelect('inscricao_da_waifu.fk_waifu');
    $query = $query->addSelect('inscricao_da_waifu.etapa AS etapa_da_inscricao');
    $query = $query->addSelect('inscricao_da_waifu.posicao_inicial');
    $query = $query->addSelect('torneio.nome AS nome_do_torneio');
    $query = $query->addSelect('torneio.quantidade_de_waifus');
    $query = $query->addSelect('torneio.status');
    $query = $query->addSelect('waifu.pk_waifu');
    $query = $query->addSelect('waifu.nome AS nome_da_waifu');
    $query = $query->addSelect('waifu.imagem AS imagem_da_waifu');

    $query = $query->join('torneio', 'inscricao_da_waifu.fk_torneio', '=', 'pk_torneio'); //INNER JOIN
    $query = $query->join('waifu', 'inscricao_da_waifu.fk_waifu', '=', 'pk_waifu'); //INNER JOIN

    $query = $query->where('inscricao_da_waifu.fk_torneio', '=', $pk_torneio);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado = array();
    foreach($array_resultado as $objeto_generico){
      $array_valores_do_banco_de_dados = (array) $objeto_generico;

      $inscricao_da_waifu = new InscricaoDaWaifu();
      $inscricao_da_waifu->set_pk_inscricao_da_waifu($array_valores_do_banco_de_dados['pk_inscricao_da_waifu']);
      $inscricao_da_waifu->set_etapa($array_valores_do_banco_de_dados['etapa_da_inscricao']);
      $inscricao_da_waifu->set_posicao_inicial($array_valores_do_banco_de_dados['posicao_inicial']);

      $torneio = new Torneio();
      $torneio->set_pk_torneio($array_valores_do_banco_de_dados['pk_torneio']);
      $torneio->set_nome($array_valores_do_banco_de_dados['nome_do_torneio']);
      $torneio->set_quantidade_de_waifus($array_valores_do_banco_de_dados['quantidade_de_waifus']);
      $torneio->set_status($array_valores_do_banco_de_dados['status']);

      $waifu = new Waifu();
      $waifu->set_pk_waifu($array_valores_do_banco_de_dados['pk_waifu']);
      $waifu->set_nome($array_valores_do_banco_de_dados['nome_da_waifu']);
      $waifu->set_imagem($array_valores_do_banco_de_dados['imagem_da_waifu']);

      $inscricao_da_waifu->set_torneio($torneio);
      $inscricao_da_waifu->set_waifu($waifu);

      $array_melhorado[] = $inscricao_da_waifu;
    }
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

  public function selecionar_pontuacoes_da_inscricao_no_torneio($pk_inscricao){
    $query = DB::table('pontuacao');
    $query = $query->addSelect('etapa');
    $query = $query->addSelect('valor');

    $query = $query->where('fk_inscricao_da_waifu', '=', $pk_inscricao);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado = array();
    foreach($array_resultado as $objeto_generico){
      $array_pontuacao = (array) $objeto_generico;
      $array_melhorado['etapa_'.$array_pontuacao['etapa']] = $array_pontuacao['valor'];
    }
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

  public function selecionar_inscricao($pk_inscricao){
    $query = DB::table('inscricao_da_waifu');
    $query = $query->addSelect('pk_inscricao_da_waifu');
    $query = $query->addSelect('inscricao_da_waifu.fk_torneio');
    $query = $query->addSelect('inscricao_da_waifu.fk_waifu');
    $query = $query->addSelect('inscricao_da_waifu.etapa AS etapa_da_inscricao');
    $query = $query->addSelect('inscricao_da_waifu.posicao_inicial');
    $query = $query->addSelect('torneio.nome');
    $query = $query->addSelect('torneio.momento_do_inicio');
    $query = $query->addSelect('torneio.quantidade_de_waifus');
    $query = $query->addSelect('torneio.etapa AS etapa_do_torneio');
    $query = $query->addSelect('torneio.status');

    $query = $query->where('pk_inscricao_da_waifu', '=', $pk_inscricao);

    $query = $query->join('torneio', 'inscricao_da_waifu.fk_torneio', '=', 'pk_torneio'); //INNER JOIN

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    if(count($array_resultado) === 0){
      $mensagem_do_model = "Nenhuma inscrição com ID $pk_inscricao foi encontrada no banco de dados";
      $mensagem_do_model .= ' do sistema.';
      $array_resultado['mensagem_do_model'] = $mensagem_do_model;
    }else{
      $array_valores_do_banco_de_dados = (array) $array_resultado[0];

      $inscricao_da_waifu = new InscricaoDaWaifu();
      $inscricao_da_waifu->set_pk_inscricao_da_waifu($array_valores_do_banco_de_dados['pk_inscricao_da_waifu']);
      $inscricao_da_waifu->set_fk_torneio($array_valores_do_banco_de_dados['fk_torneio']);
      $inscricao_da_waifu->set_fk_waifu($array_valores_do_banco_de_dados['fk_waifu']);
      $inscricao_da_waifu->set_etapa($array_valores_do_banco_de_dados['etapa_da_inscricao']);
      $inscricao_da_waifu->set_posicao_inicial($array_valores_do_banco_de_dados['posicao_inicial']);

      $torneio = new Torneio();
      $torneio->set_pk_torneio($array_valores_do_banco_de_dados['fk_torneio']);
      $torneio->set_nome($array_valores_do_banco_de_dados['nome']);
      $torneio->set_momento_do_inicio($array_valores_do_banco_de_dados['momento_do_inicio']);
      $torneio->set_quantidade_de_waifus($array_valores_do_banco_de_dados['quantidade_de_waifus']);
      $torneio->set_status($array_valores_do_banco_de_dados['status']);
      $torneio->set_etapa($array_valores_do_banco_de_dados['etapa_do_torneio']);

      $inscricao_da_waifu->set_torneio($torneio);

      $array_melhorado[] = $inscricao_da_waifu;
      $array_resultado = $array_melhorado;
    }

    return $array_resultado;
  }

  public function selecionar_pontuacao_da_waifu($pk_inscricao, $etapa){
    $query = DB::table('pontuacao');
    $query = $query->addSelect('pk_pontuacao');
    $query = $query->addSelect('fk_inscricao_da_waifu');
    $query = $query->addSelect('etapa');
    $query = $query->addSelect('valor');

    $query = $query->where('fk_inscricao_da_waifu', '=', $pk_inscricao);
    $query = $query->where('etapa', '=', $etapa);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado = array();
    if(count($array_resultado) === 0){
      $pontuacao = new Pontuacao();
      $pontuacao->set_fk_inscricao_da_waifu($pk_inscricao);
      $pontuacao->set_etapa($etapa);
      $pontuacao->set_valor(0);
    }else{
      $array_pontuacao = (array) $array_resultado[0];
      $pontuacao = new Pontuacao($array_pontuacao);
    }
    $array_melhorado[] = $pontuacao;
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

  public function selecionar_pontuacao_da_adversaria($pk_torneio, $posicoes_iniciais_das_possiveis_adversarias, $etapa){
    $query = DB::table('pontuacao');
    $query = $query->addSelect('pk_pontuacao');
    $query = $query->addSelect('fk_inscricao_da_waifu');
    $query = $query->addSelect('pontuacao.etapa');
    $query = $query->addSelect('pontuacao.valor');

    $query = $query->join('inscricao_da_waifu', 'pk_inscricao_da_waifu', '=', 'fk_inscricao_da_waifu'); //INNER JOIN

    $query = $query->where('inscricao_da_waifu.fk_torneio', '=', $pk_torneio);
    $query = $query->whereIn('inscricao_da_waifu.posicao_inicial', $posicoes_iniciais_das_possiveis_adversarias);
    $query = $query->where('pontuacao.etapa', '=', $etapa);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado = array();
    if(count($array_resultado) === 0){
      $pontuacao = new Pontuacao();
      $pontuacao->set_etapa($etapa);
      $pontuacao->set_valor(0);
    }else{
      $array_pontuacao = (array) $array_resultado[0];
      $pontuacao = new Pontuacao($array_pontuacao);
    }
    $array_melhorado[] = $pontuacao;
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

  public function votar_na_waifu_inserindo_pontuacao($pk_inscricao, $etapa){
    $insert['fk_inscricao_da_waifu'] = $pk_inscricao;
    $insert['etapa'] = $etapa;
    $insert['valor'] = 1;

    $array_resultado = array();
    try{
      DB::table('pontuacao')->insert($insert);
    }catch(Exception $excecao){
      $codigo_da_excecao = $excecao->getCode();
      switch($codigo_da_excecao){
        default:
          $array_resultado['mensagem_do_model'] = $excecao->getMessage();
          break;
      }
    }

    return $array_resultado;
  }

  public function votar_na_waifu_incrementando_o_valor($pk_pontuacao){
    $array_resultado = array();
    try{
      DB::table('pontuacao')->where('pk_pontuacao', '=', $pk_pontuacao)->increment('valor', 1);
    }catch(Exception $excecao){
      $codigo_da_excecao = $excecao->getCode();
      switch($codigo_da_excecao){
        default:
          $array_resultado['mensagem_do_model'] = $excecao->getMessage();
          break;
      }
    }

    return $array_resultado;
  }

  public function selecionar_pontuacoes_da_etapa_atual_do_torneio($pk_torneio, $etapa){
    $query = DB::table('pontuacao');
    $query = $query->addSelect('pk_pontuacao');
    $query = $query->addSelect('fk_inscricao_da_waifu');
    $query = $query->addSelect('pontuacao.etapa AS etapa_da_pontuacao');
    $query = $query->addSelect('pontuacao.valor');
    $query = $query->addSelect('pk_inscricao_da_waifu');
    $query = $query->addSelect('inscricao_da_waifu.fk_torneio');
    $query = $query->addSelect('inscricao_da_waifu.fk_waifu');
    $query = $query->addSelect('inscricao_da_waifu.etapa AS etapa_da_inscricao');
    $query = $query->addSelect('inscricao_da_waifu.posicao_inicial');

    $query = $query->rightJoin('inscricao_da_waifu', 'fk_inscricao_da_waifu', '=', 'pk_inscricao_da_waifu'); //RIGHT JOIN

    $query = $query->where('inscricao_da_waifu.fk_torneio', '=', $pk_torneio);
    $query = $query->where('inscricao_da_waifu.etapa', '=', $etapa);

    $query = $query->orderBy('posicao_inicial', 'ASC'); //Esta ordenação é importante.
    $query = $query->orderBy('pontuacao.etapa', 'DESC'); //Esta ordenação é importante.

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado = array();
    $pk_inscricao_anterior = 0;
    foreach($array_resultado as $objeto_generico){
      $array_valores_do_banco_de_dados = (array) $objeto_generico;

      $pontuacao = new Pontuacao();
      if($pk_inscricao_anterior === $array_valores_do_banco_de_dados['pk_inscricao_da_waifu']){
        continue;
      }else{
        $pk_inscricao_anterior = $array_valores_do_banco_de_dados['pk_inscricao_da_waifu'];
        if($array_valores_do_banco_de_dados['etapa_da_pontuacao'] === $etapa){
          $pontuacao->set_pk_pontuacao($array_valores_do_banco_de_dados['pk_pontuacao']);
          $pontuacao->set_fk_inscricao_da_waifu($array_valores_do_banco_de_dados['fk_inscricao_da_waifu']);
          $pontuacao->set_etapa($array_valores_do_banco_de_dados['etapa_da_pontuacao']);
          $pontuacao->set_valor($array_valores_do_banco_de_dados['valor']);
        }else{
          $pontuacao->set_fk_inscricao_da_waifu($array_valores_do_banco_de_dados['pk_inscricao_da_waifu']);
          $pontuacao->set_etapa($etapa);
          $pontuacao->set_valor(0);
        }
      }

      $inscricao_da_waifu = new InscricaoDaWaifu();
      $inscricao_da_waifu->set_pk_inscricao_da_waifu($array_valores_do_banco_de_dados['pk_inscricao_da_waifu']);
      $inscricao_da_waifu->set_fk_torneio($array_valores_do_banco_de_dados['fk_torneio']);
      $inscricao_da_waifu->set_fk_waifu($array_valores_do_banco_de_dados['fk_waifu']);
      $inscricao_da_waifu->set_etapa($array_valores_do_banco_de_dados['etapa_da_inscricao']);
      $inscricao_da_waifu->set_posicao_inicial($array_valores_do_banco_de_dados['posicao_inicial']);

      $pontuacao->set_inscricao($inscricao_da_waifu);

      $array_melhorado[] = $pontuacao;
    }
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

  public function atualizar_torneio_para_proxima_etapa($pk_torneio, $pk_waifu_vencedora = false){
    if($pk_waifu_vencedora === false){
      DB::table('torneio')->where('pk_torneio', '=', $pk_torneio)->increment('etapa', 1);
    }else{
      $update['fk_vencedora'] = $pk_waifu_vencedora;
      $update['status'] = 'torneio_encerrado';
      DB::table('torneio')->where('pk_torneio', '=', $pk_torneio)->increment('etapa', 1, $update);
    }
  }

  public function atualizar_inscricao_para_proxima_etapa($inscricao){
    $pk_inscricao = $inscricao->get_pk_inscricao_da_waifu();
    DB::table('inscricao_da_waifu')->where('pk_inscricao_da_waifu', '=', $pk_inscricao)->increment('etapa', 1);
  }

}
