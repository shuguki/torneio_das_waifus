import "./bootstrap";

import {createApp, h} from "vue";
import {createInertiaApp} from "@inertiajs/inertia-vue3";
import {resolvePageComponent} from "laravel-vite-plugin/inertia-helpers";

/* Coloque uma tag title no seu inertia.blade.php */
const titulo_do_sistema = window.document.getElementsByTagName("title")[0].innerText;

createInertiaApp({
  title: (title) => `${titulo_do_sistema} - ${title}`, //Título do sistema mais título da página
  resolve: (name) => resolvePageComponent(`./Pages/${name}.vue`, import.meta.glob("./Pages/**/*.vue")),
  setup({el, app, props, plugin}){
    el.id = "div_app_template";
    delete el.dataset.page;
    return createApp({render: () => h(app, props)}).use(plugin).mount(el);
  },
});
