import {createSSRApp, h} from "vue";
import {renderToString} from "@vue/server-renderer";
import {createInertiaApp} from "@inertiajs/inertia-vue3";
import createServer from "@inertiajs/server";
import {resolvePageComponent} from "laravel-vite-plugin/inertia-helpers";

/* Coloque uma tag title no seu inertia.blade.php */
const titulo_do_sistema = window.document.getElementsByTagName("title")[0].innerText;

createServer((page) =>
  createInertiaApp({
    page,
    render: renderToString,
    title: (title) => `${titulo_do_sistema} - ${title}`, //Título do sistema mais título da página
    resolve: (name) => resolvePageComponent(`./Pages/${name}.vue`, import.meta.glob("./Pages/**/*.vue")),
    setup({app, props, plugin}){
      return createSSRApp({render: () => h(app, props)}).use(plugin);
    },
  })
);
