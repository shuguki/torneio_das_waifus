<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;
use App\Models\Entidades\Torneio;
use App\Models\Entidades\Pontuacao;
use App\Models\Entidades\InscricaoDaWaifu;
use DateTimeZone;
use DateTime;

class Kernel extends ConsoleKernel{

  /**
   * Define the application's command schedule.
   *
   * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
   * @return void
   */
  protected function schedule(Schedule $schedule){
    $schedule->call(function (){
      $query = DB::table('torneio');
      $query = $query->addSelect('pk_torneio');
      $query = $query->addSelect('nome');
      $query = $query->addSelect('momento_do_inicio');
      $query = $query->addSelect('quantidade_de_waifus');
      $query = $query->addSelect('etapa');
      $query = $query->where('status', '=', 'torneio_iniciado');

      $colecao = $query->get();
      $array_resultado = $colecao->all();

      foreach($array_resultado as $objeto_generico){
        $array_torneio = (array) $objeto_generico;
        $torneio = new Torneio($array_torneio);

        $passar_para_proxima_etapa = false;
        $sem_fuso_horario = new DateTimeZone('GMT');
        $momento_do_inicio = new DateTime('now', $sem_fuso_horario);
        $momento_atual = new DateTime($torneio->get_momento_do_inicio(), $sem_fuso_horario);
        $tempo_decorrido = date_diff($momento_atual, $momento_do_inicio);
        switch($torneio->get_etapa()){
          case 1:
            if($tempo_decorrido->days >= 1){
              $passar_para_proxima_etapa = true;
            }
            break;
          case 2:
            if($tempo_decorrido->days >= 2){
              $passar_para_proxima_etapa = true;
            }
            break;
          case 3:
            if($tempo_decorrido->days >= 3){
              $passar_para_proxima_etapa = true;
            }
            break;
          case 4:
            if($tempo_decorrido->days >= 4){
              $passar_para_proxima_etapa = true;
            }
            break;
          case 5:
            if($tempo_decorrido->days >= 5){
              $passar_para_proxima_etapa = true;
            }
            break;
        }

        /* Torneios que iniciaram há 30 dias ou mais não passam para próxima etapa por período de 
         * tempo. Dessa forma, dados de exemplo para torneios iniciados podem continuar existindo 
         * sem se transformarem em torneios encerrados. Caso não deseje este comportamento, remova
         * o if e a atribuição abaixo.
         */
        if($tempo_decorrido->days >= 30){
          $passar_para_proxima_etapa = false;
        }

        /* Passando o torneio para a próxima etapa */
        if($passar_para_proxima_etapa){
          $query = DB::table('pontuacao');
          $query = $query->addSelect('pk_pontuacao');
          $query = $query->addSelect('fk_inscricao_da_waifu');
          $query = $query->addSelect('pontuacao.etapa AS etapa_da_pontuacao');
          $query = $query->addSelect('pontuacao.valor');
          $query = $query->addSelect('pk_inscricao_da_waifu');
          $query = $query->addSelect('inscricao_da_waifu.fk_torneio');
          $query = $query->addSelect('inscricao_da_waifu.fk_waifu');
          $query = $query->addSelect('inscricao_da_waifu.etapa AS etapa_da_inscricao');
          $query = $query->addSelect('inscricao_da_waifu.posicao_inicial');

          $query = $query->rightJoin('inscricao_da_waifu', 'fk_inscricao_da_waifu', '=', 'pk_inscricao_da_waifu'); //RIGHT JOIN

          $query = $query->where('inscricao_da_waifu.fk_torneio', '=', $torneio->get_pk_torneio());
          $query = $query->where('inscricao_da_waifu.etapa', '=', $torneio->get_etapa());

          $query = $query->orderBy('posicao_inicial', 'ASC'); //Esta ordenação é importante.
          $query = $query->orderBy('pontuacao.etapa', 'DESC'); //Esta ordenação é importante.

          $colecao = $query->get();
          $array_resultado_pontuacoes = $colecao->all();

          $array_melhorado = array();
          $pk_inscricao_anterior = 0;
          foreach($array_resultado_pontuacoes as $objeto_generico){
            $array_valores_do_banco_de_dados = (array) $objeto_generico;

            $pontuacao = new Pontuacao();
            if($pk_inscricao_anterior === $array_valores_do_banco_de_dados['pk_inscricao_da_waifu']){
              continue;
            }else{
              $pk_inscricao_anterior = $array_valores_do_banco_de_dados['pk_inscricao_da_waifu'];
              if($array_valores_do_banco_de_dados['etapa_da_pontuacao'] === $torneio->get_etapa()){
                $pontuacao->set_pk_pontuacao($array_valores_do_banco_de_dados['pk_pontuacao']);
                $pontuacao->set_fk_inscricao_da_waifu($array_valores_do_banco_de_dados['fk_inscricao_da_waifu']);
                $pontuacao->set_etapa($array_valores_do_banco_de_dados['etapa_da_pontuacao']);
                $pontuacao->set_valor($array_valores_do_banco_de_dados['valor']);
              }else{
                $pontuacao->set_fk_inscricao_da_waifu($array_valores_do_banco_de_dados['pk_inscricao_da_waifu']);
                $pontuacao->set_etapa($torneio->get_etapa());
                $pontuacao->set_valor(0);
              }
            }

            $inscricao_da_waifu = new InscricaoDaWaifu();
            $inscricao_da_waifu->set_pk_inscricao_da_waifu($array_valores_do_banco_de_dados['pk_inscricao_da_waifu']);
            $inscricao_da_waifu->set_fk_torneio($array_valores_do_banco_de_dados['fk_torneio']);
            $inscricao_da_waifu->set_fk_waifu($array_valores_do_banco_de_dados['fk_waifu']);
            $inscricao_da_waifu->set_etapa($array_valores_do_banco_de_dados['etapa_da_inscricao']);
            $inscricao_da_waifu->set_posicao_inicial($array_valores_do_banco_de_dados['posicao_inicial']);

            $pontuacao->set_inscricao($inscricao_da_waifu);

            $array_melhorado[] = $pontuacao;
          }
          $array_resultado_pontuacoes = $array_melhorado;

          $inscricoes_que_venceram = array();
          $pontuacao_anterior = null;
          $contador_de_pontuacoes = 0;
          foreach($array_resultado_pontuacoes as $pontuacao){
            $contador_de_pontuacoes++;
            if($contador_de_pontuacoes % 2 === 0){
              if($pontuacao->get_valor() > $pontuacao_anterior->get_valor()){
                $inscricoes_que_venceram[] = $pontuacao->get_inscricao();
              }elseif($pontuacao->get_valor() < $pontuacao_anterior->get_valor()){
                $inscricoes_que_venceram[] = $pontuacao_anterior->get_inscricao();
              }else{
                $sorteio = random_int(1, 2);
                if($sorteio === 1){
                  $inscricoes_que_venceram[] = $pontuacao_anterior->get_inscricao();
                }elseif($sorteio === 2){
                  $inscricoes_que_venceram[] = $pontuacao->get_inscricao();
                }
              }
            }else{
              $pontuacao_anterior = $pontuacao;
            }
          }

          /* Atualizar o torneio para a próxima etapa */
          $metade = $torneio->get_quantidade_de_waifus() / pow(2, $torneio->get_etapa());
          $quantidade_de_vencedoras = count($inscricoes_que_venceram);
          if($quantidade_de_vencedoras === $metade){
            if($metade === 1){
              $pk_waifu_vencedora = $inscricoes_que_venceram[0]->get_fk_waifu();
              $update['fk_vencedora'] = $pk_waifu_vencedora;
              $update['status'] = 'torneio_encerrado';
              DB::table('torneio')->where('pk_torneio', '=', $torneio->get_pk_torneio())->increment('etapa', 1, $update);
            }elseif($metade > 1){
              DB::table('torneio')->where('pk_torneio', '=', $torneio->get_pk_torneio())->increment('etapa', 1);
            }

            /* Atualizar as inscrições que passaram para próxima etapa */
            foreach($inscricoes_que_venceram as $inscricao){
              $pk_inscricao = $inscricao->get_pk_inscricao_da_waifu();
              DB::table('inscricao_da_waifu')->where('pk_inscricao_da_waifu', '=', $pk_inscricao)->increment('etapa', 1);
            }
          }
        }
      }
    })->name('proxima_etapa')->everyMinute();
  }

  /**
   * Register the commands for the application.
   *
   * @return void
   */
  protected function commands(){
    $this->load(__DIR__.'/Commands');

    require base_path('routes/console.php');
  }

}
