<?php

namespace App\Models\Entidades;

final class Waifu{
  private $pk_waifu;
  private $nome;
  private $imagem;

  public function __construct($array_waifu = array()){
    if(isset($array_waifu['pk_waifu'])){
      $this->pk_waifu = $array_waifu['pk_waifu'];
    }
    if(isset($array_waifu['nome'])){
      $this->nome = $array_waifu['nome'];
    }
    if(isset($array_waifu['imagem'])){
      $this->imagem = $array_waifu['imagem'];
    }
  }

  public function set_pk_waifu($pk_waifu){
    $this->pk_waifu = $pk_waifu;
  }

  public function set_nome($nome){
    $this->nome = $nome;
  }

  public function set_imagem($imagem){
    $this->imagem = $imagem;
  }

  public function get_pk_waifu(){
    return $this->pk_waifu;
  }

  public function get_nome(){
    return $this->nome;
  }

  public function get_imagem(){
    return $this->imagem;
  }

  public function quantidade_minima_de_caracteres($atributo){
    switch($atributo){
      case 'nome':
        return 2;
      case 'imagem':
        return 7;
    }
    return -1;
  }

  // O método abaixo deve ser sempre igual ou mais restritivo que o banco de dados
  public function quantidade_maxima_de_caracteres($atributo){
    switch($atributo){
      case 'nome':
        return 200;
      case 'imagem':
        return 300;
    }
    return -1;
  }

}
