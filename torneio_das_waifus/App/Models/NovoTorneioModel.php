<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use App\Models\Entidades\Waifu;
use DateTime;
use DateTimeZone;
use Exception;

final class NovoTorneioModel{

  public function verifica_disponibilidade_de_nome_do_torneio($nome){
    $query = DB::table('torneio');
    $query = $query->addSelect('nome');
    $query = $query->where('nome', '=', $nome);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    if(count($array_resultado) > 0){
      $mensagem_do_model = 'O nome escolhido já é o nome de um torneio cadastrado anteriormente.';
      $mensagem_do_model .= ' Por favor, utilize outro nome para cadastrar o torneio desejado.';
      $array_resultado['mensagem_do_model'] = $mensagem_do_model;
    }

    return $array_resultado;
  }

  public function selecionar_nomes_das_waifus($nome){
    $query = DB::table('waifu');
    $query = $query->addSelect('nome');
    $query = $query->where('nome', 'LIKE', "%$nome%");

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado = array();
    foreach($array_resultado as $objeto_generico){
      $array_melhorado[] = $objeto_generico->nome;
    }
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

  public function cadastrar_torneio($torneio){
    $insert['nome'] = $torneio->get_nome();
    $insert['quantidade_de_waifus'] = $torneio->get_quantidade_de_waifus();
    $insert['etapa'] = 0;
    $insert['status'] = 'aguardando_inscricoes';

    $array_resultado = array();
    try{
      $array_resultado['pk_torneio'] = DB::table('torneio')->insertGetId($insert);
    }catch(Exception $excecao){
      $codigo_da_excecao = $excecao->getCode();
      switch($codigo_da_excecao){
        case 1062:
          $mensagem_do_model = 'Já existe um torneio cadastrado com uma ou mais destas informações.';
          $array_resultado['mensagem_do_model'] = $mensagem_do_model;
          break;
        default:
          $array_resultado['mensagem_do_model'] = $excecao->getMessage();
          break;
      }
    }

    return $array_resultado;
  }

  public function selecionar_waifu_pelo_nome($nome){
    $query = DB::table('waifu');
    $query = $query->addSelect('pk_waifu');
    $query = $query->addSelect('nome');
    $query = $query->addSelect('imagem');
    $query = $query->where('nome', '=', $nome);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    if(count($array_resultado) === 0){
      $mensagem_do_model = 'A waifu escolhida não se encontra no banco de dados do sistema.';
      $array_resultado['mensagem_do_model'] = $mensagem_do_model;
    }else{
      $array_waifu = (array) $array_resultado[0];
      $array_resultado[0] = new Waifu($array_waifu);
    }

    return $array_resultado;
  }

  public function verificar_se_a_waifu_ja_estava_inscrita($fk_waifu, $fk_torneio){
    $query = DB::table('inscricao_da_waifu');
    $query = $query->addSelect('pk_inscricao_da_waifu');
    $query = $query->where('fk_waifu', '=', $fk_waifu);
    $query = $query->where('fk_torneio', '=', $fk_torneio);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    if(count($array_resultado) !== 0){
      $mensagem_do_model = 'Esta waifu já havia sido inscrita no torneio escolhido.';
      $array_resultado['mensagem_do_model'] = $mensagem_do_model;
      $array_resultado['esta_inscrita'] = true;
    }else{
      $array_resultado['esta_inscrita'] = false;
    }

    return $array_resultado;
  }

  public function realizar_inscricao($inscricao_da_waifu){
    $insert['fk_waifu'] = $inscricao_da_waifu->get_fk_waifu();
    $insert['fk_torneio'] = $inscricao_da_waifu->get_fk_torneio();
    $insert['etapa'] = 0;
    $insert['posicao_inicial'] = $inscricao_da_waifu->get_posicao_inicial();

    $array_resultado = array();
    try{
      DB::table('inscricao_da_waifu')->insert($insert);
    }catch(Exception $excecao){
      $codigo_da_excecao = $excecao->getCode();
      switch($codigo_da_excecao){
        default:
          $array_resultado['mensagem_do_model'] = $excecao->getMessage();
          break;
      }
    }

    return $array_resultado;
  }

  public function contar_inscricoes_do_torneio($fk_torneio){
    $query = DB::table('inscricao_da_waifu');
    $query = $query->select(DB::raw('COUNT(*) AS quantidade'));
    $query = $query->where('fk_torneio', '=', $fk_torneio);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado['quantidade'] = $array_resultado[0]->quantidade;
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

  public function iniciar_torneio($pk_torneio){
    $update['etapa'] = 1;
    $update['status'] = 'torneio_iniciado';

    $sem_fuso_horario = new DateTimeZone('GMT');
    $objeto_date_time = new DateTime('now', $sem_fuso_horario);
    $update['momento_do_inicio'] = $objeto_date_time->format('Y-m-d H:i:s');

    $array_resultado = array();
    try{
      DB::table('torneio')->where('pk_torneio', '=', $pk_torneio)->update($update);
    }catch(Exception $excecao){
      $codigo_da_excecao = $excecao->getCode();
      switch($codigo_da_excecao){
        default:
          $array_resultado['mensagem_do_model'] = $excecao->getMessage();
          break;
      }
    }

    return $array_resultado;
  }

  public function atualizar_inscricoes_do_torneio($fk_torneio){
    $update['etapa'] = 1;

    $array_resultado = array();
    try{
      DB::table('inscricao_da_waifu')->where('fk_torneio', '=', $fk_torneio)->where('etapa', '<', 1)->update($update);
    }catch(Exception $excecao){
      $codigo_da_excecao = $excecao->getCode();
      switch($codigo_da_excecao){
        default:
          $array_resultado['mensagem_do_model'] = $excecao->getMessage();
          break;
      }
    }

    return $array_resultado;
  }

}
