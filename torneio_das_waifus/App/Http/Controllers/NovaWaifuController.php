<?php

namespace App\Http\Controllers;

use App\Models\NovaWaifuModel;
use App\Models\Entidades\Waifu;
use Inertia\Inertia;

final class NovaWaifuController extends TemplateLayoutController{

  public function carregar_pagina($redirecionar = false){
    if($redirecionar){
      //Redireciona para si mesmo, motivo: limpar a requisição.
      header('Location: /nova_waifu');
      die;
    }

    $valores = $this->valores_do_template_layout();
    $sessao = session();

    /* Colocando valores iniciais nas variáveis para não ficarem undefined no Vue */
    $valores['nova_waifu']['mensagem'] = '';
    $valores['nova_waifu']['tipo_de_mensagem'] = 'sucesso';

    /* Recolocando valores preenchidos previamente pelo usuário no formulário */
    if($sessao->has('backup_do_formulario_da_pagina_nova_waifu')){
      $backup = $sessao->get('backup_do_formulario_da_pagina_nova_waifu');
      $valores['nova_waifu']['nome'] = $backup['nome'];
      $valores['nova_waifu']['imagem'] = $backup['imagem'];
      $valores['nova_waifu']['tipo_de_mensagem'] = 'falha';
      $sessao->forget('backup_do_formulario_da_pagina_nova_waifu');
      $sessao->save();
    }else{
      $valores['nova_waifu']['nome'] = '';
      $valores['nova_waifu']['imagem'] = '';
    }

    /* Se houver mensagem na sessão, deve ser mostrada */
    if($sessao->has('mensagem_da_pagina_nova_waifu')){
      $mensagem = $sessao->get('mensagem_da_pagina_nova_waifu');
      $valores['nova_waifu']['mensagem'] = $mensagem;
      $sessao->forget('mensagem_da_pagina_nova_waifu');
      $sessao->save();
    }

    return Inertia::render('nova_waifu/nova_waifu', $valores);
  }

  public function cadastrar_waifu(){
    $sessao = session();

    $nova_waifu_model = new NovaWaifuModel();

    $waifu = new Waifu();

    /* Obtendo valores do formulário */
    $requisicao = $this->get_requisicao();
    $nome = trim($requisicao->post('nome') ?? '');
    $imagem = trim($requisicao->post('imagem') ?? '');

    while(strpos($nome, '  ') !== false){
      $nome = str_replace('  ', ' ', $nome);
    }

    $backup_do_formulario['nome'] = $nome;
    $backup_do_formulario['imagem'] = $imagem;
    $sessao->put('backup_do_formulario_da_pagina_nova_waifu', $backup_do_formulario);
    $sessao->save();

    /* Validações */
    if($nome === ''){
      $mensagem = 'A waifu não foi cadastrada.';
      $mensagem .= ' O campo nome da waifu precisa ser preenchido.';
      $sessao->put('mensagem_da_pagina_nova_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    $array_resultado = $nova_waifu_model->verifica_disponibilidade_de_nome_da_waifu($nome);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = 'A waifu não foi cadastrada.';
      $mensagem .= ' '.$array_resultado['mensagem_do_model'];
      $sessao->put('mensagem_da_pagina_nova_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    $minimo = $waifu->quantidade_minima_de_caracteres('nome');
    $maximo = $waifu->quantidade_maxima_de_caracteres('nome');
    $quantidade = mb_strlen($nome);
    if($quantidade < $minimo){
      $mensagem = 'A waifu não foi cadastrada.';
      $mensagem .= " O campo nome da waifu precisa ter no mínimo $minimo caracteres.";
      $sessao->put('mensagem_da_pagina_nova_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    if($quantidade > $maximo){
      $mensagem = 'A waifu não foi cadastrada.';
      $mensagem .= " O campo nome da waifu não pode ultrapassar $maximo caracteres.";
      $sessao->put('mensagem_da_pagina_nova_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }

    if($imagem === ''){
      $mensagem = 'A waifu não foi cadastrada.';
      $mensagem .= ' O campo imagem da waifu precisa ser preenchido.';
      $sessao->put('mensagem_da_pagina_nova_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    $minimo = $waifu->quantidade_minima_de_caracteres('imagem');
    $maximo = $waifu->quantidade_maxima_de_caracteres('imagem');
    $quantidade = mb_strlen($imagem);
    if($quantidade < $minimo){
      $mensagem = 'A waifu não foi cadastrada.';
      $mensagem .= " O campo imagem da waifu precisa ter no mínimo $minimo caracteres.";
      $sessao->put('mensagem_da_pagina_nova_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    if($quantidade > $maximo){
      $mensagem = 'A waifu não foi cadastrada.';
      $mensagem .= " O campo imagem da waifu não pode ultrapassar $maximo caracteres.";
      $sessao->put('mensagem_da_pagina_nova_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    $inicio = mb_substr($imagem, 0, 20);
    if($inicio != 'https://i.imgur.com/'){
      $mensagem = 'A waifu não foi cadastrada.';
      $mensagem .= ' O campo imagem da waifu precisa começar com https://i.imgur.com/';
      $sessao->put('mensagem_da_pagina_nova_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
    $final = mb_substr($imagem, 20);
    $combinacoes = array();
    if(!preg_match('/^[a-z, 0-9]{7,8}(\.[a-z, 0-9]{3,4})$/i', $final, $combinacoes)){
      $mensagem = 'A waifu não foi cadastrada.';
      $mensagem .= ' O campo imagem da waifu precisa ter um Direct Link do Imgur.';
      $sessao->put('mensagem_da_pagina_nova_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }elseif(!in_array($combinacoes[1], ['.png', '.jpg', '.gif'])){
      $mensagem = 'A waifu não foi cadastrada.';
      $mensagem .= ' O campo imagem da waifu precisa terminar com um dos formatos em minúsculo:';
      $mensagem .= ' .png, .jpg ou .gif.';
      $sessao->put('mensagem_da_pagina_nova_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }

    /* Cadastrar waifu no banco de dados */
    $waifu->set_nome($nome);
    $waifu->set_imagem($imagem);
    $array_resultado = $nova_waifu_model->cadastrar_waifu($waifu);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = 'A waifu não foi cadastrada.';
      $mensagem .= ' '.$array_resultado['mensagem_do_model'];
      $sessao->put('mensagem_da_pagina_nova_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }else{
      $mensagem = 'A waifu foi cadastrada com sucesso.';
      $sessao->put('mensagem_da_pagina_nova_waifu', $mensagem);
      $sessao->forget('backup_do_formulario_da_pagina_nova_waifu');
      $sessao->save();
      $this->carregar_pagina(true);
      die;
    }
  }

}
