<?php

namespace App\Http\Controllers;

use App\Models\InscreverWaifuModel;
use App\Models\Entidades\InscricaoDaWaifu;
use Inertia\Inertia;

final class InscreverWaifuController extends TemplateLayoutController{
  private const QUANTIDADE_PADRAO_POR_PAGINA = 16;

  public function carregar_pagina($redirecionar_com_id = false){
    if($redirecionar_com_id !== false){
      //Redireciona para si mesmo, motivo: limpar a requisição.
      header("Location: /inscrever_waifu?id=$redirecionar_com_id");
      die;
    }

    $valores = $this->valores_do_template_layout();
    $sessao = session();

    /* Colocando valores iniciais nas variáveis para não ficarem undefined no Vue */
    $valores['inscrever_waifu']['mensagem'] = '';
    $valores['inscrever_waifu']['tipo_de_mensagem'] = 'sucesso';
    $valores['inscrever_waifu']['id_valido'] = true;
    $valores['inscrever_waifu']['id'] = '';
    $valores['inscrever_waifu']['nome'] = '';
    $valores['inscrever_waifu']['imagem'] = '';
    $valores['inscrever_waifu']['torneios_aguardando_inscricoes'] = array();

    $inscrever_waifu_model = new InscreverWaifuModel();

    /* Validando o ID da waifu informado na URL */
    $requisicao = $this->get_requisicao();
    $pk_waifu = $requisicao->get('id');
    if(!is_numeric($pk_waifu) or $pk_waifu <= 0 or floor($pk_waifu) != $pk_waifu){
      $mensagem = 'ID inválido, o ID da waifu precisa ser um número natural maior que zero.';
      $valores['inscrever_waifu']['mensagem'] = $mensagem;
      $valores['inscrever_waifu']['tipo_de_mensagem'] = 'falha';
      $valores['inscrever_waifu']['id_valido'] = false;
    }else{
      /* Consultando e mostrando informações da waifu */
      $array_resultado = $inscrever_waifu_model->selecionar_waifu($pk_waifu);
      if(isset($array_resultado['mensagem_do_model'])){
        $valores['inscrever_waifu']['mensagem'] = $array_resultado['mensagem_do_model'];
        $valores['inscrever_waifu']['tipo_de_mensagem'] = 'falha';
        $valores['inscrever_waifu']['id_valido'] = false;
      }else{
        $waifu = $array_resultado[0];

        $valores['inscrever_waifu']['id'] = $waifu->get_pk_waifu();

        $nome = $waifu->get_nome();
        $valores['inscrever_waifu']['nome'] = $nome;

        $imagem = $waifu->get_imagem();
        $valores['inscrever_waifu']['imagem'] = $imagem;

        /* Chamando método para mostrar os torneios que estão aguardando inscrições */
        $valores['inscrever_waifu']['torneios_aguardando_inscricoes'] = $this->mostrar_torneios_que_estao_aguardando_inscricoes($pk_waifu);
      }
    }

    /* Se houver mensagem na sessão, deve ser mostrada */
    if($sessao->has('mensagem_da_pagina_inscrever_waifu')){
      $valores['inscrever_waifu']['mensagem'] = $sessao->get('mensagem_da_pagina_inscrever_waifu');
      if($sessao->has('tipo_de_mensagem_da_pagina_inscrever_waifu')){
        $valores['inscrever_waifu']['tipo_de_mensagem'] = $sessao->get('tipo_de_mensagem_da_pagina_inscrever_waifu');
      }
      $sessao->forget('mensagem_da_pagina_inscrever_waifu');
      $sessao->save();
    }

    return Inertia::render('inscrever_waifu/inscrever_waifu', $valores);
  }

  private function mostrar_torneios_que_estao_aguardando_inscricoes($pk_waifu){
    $inscrever_waifu_model = new InscreverWaifuModel();

    $valores_deste_metodo = array();

    $requisicao = $this->get_requisicao();

    /* Preparando os filtros */
    $filtros = array();
    $nome_do_torneio = trim($requisicao->get('filtro_nome_do_torneio') ?? '');
    if($nome_do_torneio !== ''){
      $filtros['nome_do_torneio'] = $nome_do_torneio;
    }
    $valores_deste_metodo['filtro_nome_do_torneio'] = $nome_do_torneio;

    /* Preparando a ordenação */
    $ordenacao = $requisicao->get('ordenacao');
    switch($ordenacao){
      case 'padrao':
      case 'nome_a_z':
      case 'nome_z_a':
      case 'vagas_disponiveis_crescente':
      case 'vagas_disponiveis_decrescente':
      case 'opcoes_a_z':
      case 'opcoes_z_a':
        break;
      default:
        $ordenacao = 'padrao';
        break;
    }
    $valores_deste_metodo['ordenacao'] = $ordenacao;

    /* Preparando a paginação */
    $quantidade_por_pagina = self::QUANTIDADE_PADRAO_POR_PAGINA;

    $pagina = (int) $requisicao->get('pagina');
    if($pagina < 1){
      $pagina = 1;
    }
    $quantidade_de_paginas = $this->calcular_quantidade_de_paginas_dos_torneios_que_estao_aguardando_inscricoes($filtros, $quantidade_por_pagina);
    if($pagina > $quantidade_de_paginas){
      $pagina = $quantidade_de_paginas;
    }

    $valores_deste_metodo['pagina_atual'] = $pagina;
    $valores_deste_metodo['ultima_pagina'] = $quantidade_de_paginas;

    $descartar = $quantidade_por_pagina * $pagina - $quantidade_por_pagina;
    $descartar = max($descartar, 0);

    /* Preparando o resultado */
    $torneios = $inscrever_waifu_model->selecionar_torneios_que_estao_aguardando_inscricoes($pk_waifu, $filtros, $ordenacao, $quantidade_por_pagina, $descartar);
    $array_torneios = array();

    foreach($torneios as $torneio){
      $array_torneio = array();

      $pk_torneio = $torneio->get_pk_torneio();
      $array_torneio['id'] = $pk_torneio;

      $nome = $torneio->get_nome();
      $array_torneio['nome'] = $nome;

      $quantidade_de_waifus = $torneio->get_quantidade_de_waifus();

      $quantidade_de_vagas_disponiveis = $quantidade_de_waifus - $torneio->get_quantidade_de_waifus_inscritas();
      if($quantidade_de_vagas_disponiveis < 0){
        $quantidade_de_vagas_disponiveis = 0;
      }
      $array_torneio['quantidade_de_vagas_disponiveis'] = $quantidade_de_vagas_disponiveis;

      $array_resultado = $inscrever_waifu_model->verificar_se_a_waifu_ja_estava_inscrita($pk_waifu, $pk_torneio);
      $array_torneio['esta_inscrita'] = $array_resultado['esta_inscrita'];

      $array_torneios[] = $array_torneio;
    }
    $valores_deste_metodo['lista'] = $array_torneios;

    return $valores_deste_metodo;
  }

  public function mostrar_torneios_que_estao_aguardando_inscricoes_ajax(){
    $inscrever_waifu_model = new InscreverWaifuModel();

    $retorno = array();

    /* Validando o ID da waifu informado na URL */
    $requisicao = $this->get_requisicao();
    $pk_waifu = $requisicao->get('id');
    if(!is_numeric($pk_waifu) or $pk_waifu <= 0 or floor($pk_waifu) != $pk_waifu){
      $mensagem = 'ID inválido, o ID da waifu precisa ser um número natural maior que zero.';
      $retorno['mensagem'] = $mensagem;
      echo json_encode($retorno);
      die;
    }else{
      $array_resultado = $inscrever_waifu_model->selecionar_waifu($pk_waifu);
      if(isset($array_resultado['mensagem_do_model'])){
        $retorno['mensagem'] = $array_resultado['mensagem_do_model'];
        echo json_encode($retorno);
        die;
      }
    }

    $retorno = $this->mostrar_torneios_que_estao_aguardando_inscricoes($pk_waifu);
    echo json_encode($retorno);
  }

  private function calcular_quantidade_de_paginas_dos_torneios_que_estao_aguardando_inscricoes($filtros, $quantidade_por_pagina){
    $inscrever_waifu_model = new InscreverWaifuModel();

    $array_resultado = $inscrever_waifu_model->contar_torneios_que_estao_aguardando_inscricoes($filtros);
    $quantidade_de_paginas = ceil($array_resultado['quantidade'] / $quantidade_por_pagina);

    return $quantidade_de_paginas;
  }

  public function inscrever(){
    $inscrever_waifu_model = new InscreverWaifuModel();

    $sessao = session();

    /* Indicando que a mensagem pode ser de falha */
    $sessao->put('tipo_de_mensagem_da_pagina_inscrever_waifu', 'falha');
    $torneio_esta_aceitando_inscricoes = false;
    $quantidade_total_de_vagas = 0;
    $quantidade_de_waifus_que_ja_estavam_inscritas = 0;

    $requisicao = $this->get_requisicao();

    /* Validações */
    $pk_waifu = $requisicao->get('id');
    if(!is_numeric($pk_waifu) or $pk_waifu <= 0 or floor($pk_waifu) != $pk_waifu){
      $mensagem = 'A waifu não foi inscrita.';
      $mensagem .= ' O ID da waifu precisa ser um número natural maior que zero.';
      $sessao->put('mensagem_da_pagina_inscrever_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina($pk_waifu);
      die;
    }else{
      $array_resultado = $inscrever_waifu_model->selecionar_waifu($pk_waifu);
      if(isset($array_resultado['mensagem_do_model'])){
        $mensagem = 'A waifu não foi inscrita.';
        $mensagem .= " {$array_resultado['mensagem_do_model']}";
        $sessao->put('mensagem_da_pagina_inscrever_waifu', $mensagem);
        $sessao->save();
        $this->carregar_pagina($pk_waifu);
        die;
      }
    }

    $pk_torneio = $requisicao->get('id_do_torneio');
    if(!is_numeric($pk_torneio) or $pk_torneio <= 0 or floor($pk_torneio) != $pk_torneio){
      $mensagem = 'A waifu não foi inscrita.';
      $mensagem .= ' O ID do torneio precisa ser um número natural maior que zero.';
      $sessao->put('mensagem_da_pagina_inscrever_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina($pk_waifu);
      die;
    }else{
      $array_resultado = $inscrever_waifu_model->selecionar_torneio($pk_torneio);
      if(isset($array_resultado['mensagem_do_model'])){
        $mensagem = 'A waifu não foi inscrita.';
        $mensagem .= " {$array_resultado['mensagem_do_model']}";
        $sessao->put('mensagem_da_pagina_inscrever_waifu', $mensagem);
        $sessao->save();
        $this->carregar_pagina($pk_waifu);
        die;
      }else{
        $torneio = $array_resultado[0];
        if($torneio->get_status() === 'aguardando_inscricoes'){
          $torneio_esta_aceitando_inscricoes = true;
        }
        $quantidade_total_de_vagas = $torneio->get_quantidade_de_waifus();
      }
    }

    $array_resultado = $inscrever_waifu_model->verificar_se_a_waifu_ja_estava_inscrita($pk_waifu, $pk_torneio);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = 'A waifu não foi inscrita.';
      $mensagem .= " {$array_resultado['mensagem_do_model']}";
      $sessao->put('mensagem_da_pagina_inscrever_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina($pk_waifu);
      die;
    }

    if(!$torneio_esta_aceitando_inscricoes){
      $mensagem = 'A waifu não foi inscrita.';
      $mensagem .= ' O torneio escolhido não está mais na etapa de aceitar inscrições.';
      $sessao->put('mensagem_da_pagina_inscrever_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina($pk_waifu);
      die;
    }

    $array_posicoes_disponiveis = array();
    for($posicao = 1; $posicao <= $quantidade_total_de_vagas; $posicao++){
      $array_posicoes_disponiveis[] = $posicao;
    }

    $inscricoes = $inscrever_waifu_model->selecionar_inscricoes_do_torneio($pk_torneio);
    foreach($inscricoes as $inscricao){
      $quantidade_de_waifus_que_ja_estavam_inscritas++;

      $posicao_desta_inscrita = $inscricao->get_posicao_inicial();
      $chave_da_posicao_ocupada = $posicao_desta_inscrita - 1;
      unset($array_posicoes_disponiveis[$chave_da_posicao_ocupada]);
    }

    if($quantidade_de_waifus_que_ja_estavam_inscritas >= $quantidade_total_de_vagas){
      $mensagem = 'A waifu não foi inscrita.';
      $mensagem .= ' O torneio escolhido não possui mais vagas abertas para inscrições.';
      $sessao->put('mensagem_da_pagina_inscrever_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina($pk_waifu);
      die;
    }

    /* Realizando inscrição da waifu no torneio */
    $inscricao_da_waifu = new InscricaoDaWaifu();
    $inscricao_da_waifu->set_fk_waifu($pk_waifu);
    $inscricao_da_waifu->set_fk_torneio($pk_torneio);
    $posicao_sorteada = $array_posicoes_disponiveis[array_rand($array_posicoes_disponiveis)];
    $inscricao_da_waifu->set_posicao_inicial($posicao_sorteada);
    $array_resultado = $inscrever_waifu_model->realizar_inscricao($inscricao_da_waifu);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = 'A waifu não foi inscrita.';
      $mensagem .= " {$array_resultado['mensagem_do_model']}";
      $sessao->put('mensagem_da_pagina_inscrever_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina($pk_waifu);
      die;
    }else{
      $mensagem = 'A waifu foi inscrita com sucesso';

      /* Atualizar etapa e status do torneio automaticamente */
      if($quantidade_de_waifus_que_ja_estavam_inscritas + 1 >= $quantidade_total_de_vagas){
        $array_resultado = $inscrever_waifu_model->iniciar_torneio($pk_torneio);
        if(isset($array_resultado['mensagem_do_model'])){
          $mensagem .= ', porém não foi possível iniciar o torneio. Devido ao erro:';
          $mensagem .= " {$array_resultado['mensagem_do_model']}.";
          $mensagem .= ' Contate um administrador do sistema para que o torneio possa iniciar';
        }else{
          $array_resultado = $inscrever_waifu_model->atualizar_inscricoes_do_torneio($pk_torneio);
          if(isset($array_resultado['mensagem_do_model'])){
            $mensagem .= ', porém o seguinte erro ocorreu:';
            $mensagem .= " {$array_resultado['mensagem_do_model']}.";
            $mensagem .= ' Reporte o erro para um administrador do sistema';
          }
        }
      }
      $mensagem .= '.';

      $sessao->put('mensagem_da_pagina_inscrever_waifu', $mensagem);
      $sessao->put('tipo_de_mensagem_da_pagina_inscrever_waifu', 'sucesso');
      $sessao->save();

      $this->carregar_pagina($pk_waifu);
      die;
    }
  }

}
