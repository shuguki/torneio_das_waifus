<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use App\Models\Entidades\Waifu;

final class WaifusModel{

  public function selecionar_waifus($filtros, $ordenacao, $quantidade, $descartar){
    $query = DB::table('waifu');
    $query = $query->addSelect('pk_waifu');
    $query = $query->addSelect('nome');
    $query = $query->addSelect('imagem');

    foreach($filtros as $chave => $valor){
      switch($chave){
        case 'nome_da_waifu':
          $query = $query->where('nome', 'LIKE', "%$valor%");
          break;
      }
    }

    switch($ordenacao){
      case 'padrao':
        $query = $query->orderBy('nome', 'ASC');
        break;
      case 'nome_a_z':
        $query = $query->orderBy('nome', 'ASC');
        break;
      case 'nome_z_a':
        $query = $query->orderBy('nome', 'DESC');
        break;
    }

    $query = $query->offset($descartar);
    $query = $query->limit($quantidade);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado = array();
    foreach($array_resultado as $objeto_generico){
      $array_waifu = (array) $objeto_generico;
      $waifu = new Waifu($array_waifu);
      $array_melhorado[] = $waifu;
    }
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

  public function contar_waifus($filtros){
    $query = DB::table('waifu');
    $query = $query->select(DB::raw('COUNT(*) AS quantidade'));

    foreach($filtros as $chave => $valor){
      switch($chave){
        case 'nome_da_waifu':
          $query = $query->where('nome', 'LIKE', "%$valor%");
          break;
      }
    }

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado['quantidade'] = $array_resultado[0]->quantidade;
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

}
