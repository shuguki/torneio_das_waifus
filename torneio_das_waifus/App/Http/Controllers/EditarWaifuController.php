<?php

namespace App\Http\Controllers;

use App\Models\EditarWaifuModel;
use App\Models\Entidades\Waifu;
use Inertia\Inertia;

final class EditarWaifuController extends TemplateLayoutController{

  public function carregar_pagina($redirecionar_com_id = false){
    if($redirecionar_com_id !== false){
      //Redireciona para si mesmo, motivo: limpar a requisição.
      header("Location: /editar_waifu?id=$redirecionar_com_id");
      die;
    }

    $valores = $this->valores_do_template_layout();
    $sessao = session();

    /* Colocando valores iniciais nas variáveis para não ficarem undefined no Vue */
    $valores['editar_waifu']['mensagem'] = '';
    $valores['editar_waifu']['tipo_de_mensagem'] = 'sucesso';
    $valores['editar_waifu']['id_valido'] = true;
    $valores['editar_waifu']['id'] = '';
    $valores['editar_waifu']['nome'] = '';
    $valores['editar_waifu']['imagem'] = '';
    $valores['editar_waifu']['nome_no_formulario'] = '';
    $valores['editar_waifu']['imagem_no_formulario'] = '';

    $editar_waifu_model = new EditarWaifuModel();

    /* Validando o ID da waifu informado na URL */
    $requisicao = $this->get_requisicao();
    $pk_waifu = $requisicao->get('id');
    if(!is_numeric($pk_waifu) or $pk_waifu <= 0 or floor($pk_waifu) != $pk_waifu){
      $mensagem = 'ID inválido, o ID da waifu precisa ser um número natural maior que zero.';
      $valores['editar_waifu']['mensagem'] = $mensagem;
      $valores['editar_waifu']['tipo_de_mensagem'] = 'falha';
      $valores['editar_waifu']['id_valido'] = false;
    }else{
      /* Consultando e mostrando informações da waifu */
      $array_resultado = $editar_waifu_model->selecionar_waifu($pk_waifu);
      if(isset($array_resultado['mensagem_do_model'])){
        $valores['editar_waifu']['mensagem'] = $array_resultado['mensagem_do_model'];
        $valores['editar_waifu']['tipo_de_mensagem'] = 'falha';
        $valores['editar_waifu']['id_valido'] = false;
      }else{
        $waifu = $array_resultado[0];
        $valores['editar_waifu']['id'] = $waifu->get_pk_waifu();
        $nome = $waifu->get_nome();
        $imagem = $waifu->get_imagem();
        $valores['editar_waifu']['nome'] = $nome;
        $valores['editar_waifu']['imagem'] = $imagem;
        $valores['editar_waifu']['nome_no_formulario'] = $nome;
        $valores['editar_waifu']['imagem_no_formulario'] = $imagem;
      }
    }

    /* Recolocando valores preenchidos previamente pelo usuário no formulário */
    if($sessao->has('backup_do_formulario_da_pagina_editar_waifu')){
      $backup = $sessao->get('backup_do_formulario_da_pagina_editar_waifu');
      $valores['editar_waifu']['nome_no_formulario'] = $backup['nome'];
      $valores['editar_waifu']['imagem_no_formulario'] = $backup['imagem'];
      $valores['editar_waifu']['tipo_de_mensagem'] = 'falha';
      $sessao->forget('backup_do_formulario_da_pagina_editar_waifu');
      $sessao->save();
    }

    /* Se houver mensagem na sessão, deve ser mostrada */
    if($sessao->has('mensagem_da_pagina_editar_waifu')){
      $valores['editar_waifu']['mensagem'] = $sessao->get('mensagem_da_pagina_editar_waifu');
      $sessao->forget('mensagem_da_pagina_editar_waifu');
      $sessao->save();
    }

    return Inertia::render('editar_waifu/editar_waifu', $valores);
  }

  public function editar_waifu(){
    $editar_waifu_model = new EditarWaifuModel();

    $waifu = new Waifu();

    $sessao = session();

    /* Obtendo valores do formulário */
    $requisicao = $this->get_requisicao();
    $pk_waifu = $requisicao->post('id');
    $nome = trim($requisicao->post('nome') ?? '');
    $imagem = trim($requisicao->post('imagem') ?? '');

    while(strpos($nome, '  ') !== false){
      $nome = str_replace('  ', ' ', $nome);
    }

    $backup_do_formulario['pk_waifu'] = $pk_waifu;
    $backup_do_formulario['nome'] = $nome;
    $backup_do_formulario['imagem'] = $imagem;
    $sessao->put('backup_do_formulario_da_pagina_editar_waifu', $backup_do_formulario);
    $sessao->save();

    /* Validações */
    if(!is_numeric($pk_waifu) or $pk_waifu <= 0 or floor($pk_waifu) != $pk_waifu){
      $mensagem = 'A waifu não foi editada.';
      $mensagem .= ' O ID da waifu precisa ser um número natural maior que zero.';
      $sessao->put('mensagem_da_pagina_editar_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina($pk_waifu);
      die;
    }
    $array_resultado = $editar_waifu_model->selecionar_waifu($pk_waifu);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = 'A waifu não foi editada.';
      $mensagem .= " {$array_resultado['mensagem_do_model']}";
      $sessao->put('mensagem_da_pagina_editar_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina($pk_waifu);
      die;
    }

    if($nome === ''){
      $mensagem = 'A waifu não foi editada.';
      $mensagem .= ' O campo nome da waifu não pode ficar em branco.';
      $sessao->put('mensagem_da_pagina_editar_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina($pk_waifu);
      die;
    }
    $array_resultado = $editar_waifu_model->verificar_disponibilidade_de_nome($pk_waifu, $nome);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = 'A waifu não foi editada.';
      $mensagem .= " {$array_resultado['mensagem_do_model']}";
      $sessao->put('mensagem_da_pagina_editar_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina($pk_waifu);
      die;
    }
    $minimo = $waifu->quantidade_minima_de_caracteres('nome');
    $maximo = $waifu->quantidade_maxima_de_caracteres('nome');
    $quantidade = mb_strlen($nome);
    if($quantidade < $minimo){
      $mensagem = 'A waifu não foi editada.';
      $mensagem .= " O campo nome da waifu precisa ter no mínimo $minimo caracteres.";
      $sessao->put('mensagem_da_pagina_editar_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina($pk_waifu);
      die;
    }
    if($quantidade > $maximo){
      $mensagem = 'A waifu não foi editada.';
      $mensagem .= " O campo nome da waifu não pode ultrapassar $maximo caracteres.";
      $sessao->put('mensagem_da_pagina_editar_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina($pk_waifu);
      die;
    }

    if($imagem === ''){
      $mensagem = 'A waifu não foi editada.';
      $mensagem .= ' O campo imagem da waifu precisa ser preenchido.';
      $sessao->put('mensagem_da_pagina_editar_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina($pk_waifu);
      die;
    }
    $minimo = $waifu->quantidade_minima_de_caracteres('imagem');
    $maximo = $waifu->quantidade_maxima_de_caracteres('imagem');
    $quantidade = mb_strlen($imagem);
    if($quantidade < $minimo){
      $mensagem = 'A waifu não foi editada.';
      $mensagem .= " O campo imagem da waifu precisa ter no mínimo $minimo caracteres.";
      $sessao->put('mensagem_da_pagina_editar_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina($pk_waifu);
      die;
    }
    if($quantidade > $maximo){
      $mensagem = 'A waifu não foi editada.';
      $mensagem .= " O campo imagem da waifu não pode ultrapassar $maximo caracteres.";
      $sessao->put('mensagem_da_pagina_editar_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina($pk_waifu);
      die;
    }
    $inicio = mb_substr($imagem, 0, 20);
    if($inicio != 'https://i.imgur.com/'){
      $mensagem = 'A waifu não foi editada.';
      $mensagem .= ' O campo imagem da waifu precisa começar com https://i.imgur.com/';
      $sessao->put('mensagem_da_pagina_editar_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina($pk_waifu);
      die;
    }
    $final = mb_substr($imagem, 20);
    $combinacoes = array();
    if(!preg_match('/^[a-z, 0-9]{7,8}(\.[a-z, 0-9]{3,4})$/i', $final, $combinacoes)){
      $mensagem = 'A waifu não foi editada.';
      $mensagem .= ' O campo imagem da waifu precisa ter um Direct Link do Imgur.';
      $sessao->put('mensagem_da_pagina_editar_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina($pk_waifu);
      die;
    }elseif(!in_array($combinacoes[1], ['.png', '.jpg', '.gif'])){
      $mensagem = 'A waifu não foi editada.';
      $mensagem .= ' O campo imagem da waifu precisa terminar com um dos formatos em minúsculo:';
      $mensagem .= ' .png, .jpg ou .gif.';
      $sessao->put('mensagem_da_pagina_editar_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina($pk_waifu);
      die;
    }

    /* Editar waifu no banco de dados */
    $waifu->set_pk_waifu($pk_waifu);
    $waifu->set_nome($nome);
    $waifu->set_imagem($imagem);
    $array_resultado = $editar_waifu_model->editar_waifu($waifu);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = 'A waifu não foi editada.';
      $mensagem .= " {$array_resultado['mensagem_do_model']}";
      $sessao->put('mensagem_da_pagina_editar_waifu', $mensagem);
      $sessao->save();
      $this->carregar_pagina($pk_waifu);
      die;
    }else{
      $mensagem = 'A waifu foi editada com sucesso.';
      $sessao->put('mensagem_da_pagina_editar_waifu', $mensagem);
      $sessao->forget('backup_do_formulario_da_pagina_editar_waifu');
      $sessao->save();
      $this->carregar_pagina($pk_waifu);
      die;
    }
  }

}
