<?php

namespace App\Http\Controllers;

use App\Models\TorneiosModel;
use Inertia\Inertia;
use DateTime;
use DateTimeZone;

final class TorneiosController extends TemplateLayoutController{
  private const QUANTIDADE_PADRAO_POR_PAGINA = 4;

  public function carregar_pagina($redirecionar = false){
    if($redirecionar){
      //Redireciona para si mesmo, motivo: limpar a requisição.
      header('Location: /torneios');
      die;
    }

    $valores = $this->valores_do_template_layout();

    $valores['torneios']['aguardando_inscricoes'] = $this->mostrar_torneios_que_estao_aguardando_inscricoes();
    $valores['torneios']['iniciados'] = $this->mostrar_torneios_iniciados();
    $valores['torneios']['encerrados'] = $this->mostrar_torneios_encerrados();

    return Inertia::render('torneios/torneios', $valores);
  }

  private function mostrar_torneios_que_estao_aguardando_inscricoes(){
    $torneios_model = new TorneiosModel();

    $valores_deste_metodo = array();

    $requisicao = $this->get_requisicao();

    /* Preparando os filtros */
    $filtros = array();
    $nome_do_torneio = trim($requisicao->get('filtro_nome_do_torneio') ?? '');
    if($nome_do_torneio !== ''){
      $filtros['nome_do_torneio'] = $nome_do_torneio;
    }
    $valores_deste_metodo['filtro_nome_do_torneio'] = $nome_do_torneio;

    /* Preparando a ordenação */
    $ordenacao = $requisicao->get('ordenacao');
    switch($ordenacao){
      case 'padrao':
      case 'nome_a_z':
      case 'nome_z_a':
      case 'vagas_disponiveis_crescente':
      case 'vagas_disponiveis_decrescente':
      case 'total_de_vagas_crescente':
      case 'total_de_vagas_decrescente':
        break;
      default:
        $ordenacao = 'padrao';
        break;
    }
    $valores_deste_metodo['ordenacao'] = $ordenacao;

    /* Preparando a paginação */
    $quantidade_por_pagina = self::QUANTIDADE_PADRAO_POR_PAGINA;

    $pagina = (int) $requisicao->get('pagina');
    if($pagina < 1){
      $pagina = 1;
    }
    $quantidade_de_paginas = $this->calcular_quantidade_de_paginas_dos_torneios_que_estao_aguardando_inscricoes($filtros, $quantidade_por_pagina);
    if($pagina > $quantidade_de_paginas){
      $pagina = $quantidade_de_paginas;
    }

    $valores_deste_metodo['pagina_atual'] = $pagina;
    $valores_deste_metodo['ultima_pagina'] = $quantidade_de_paginas;

    $descartar = $quantidade_por_pagina * $pagina - $quantidade_por_pagina;
    $descartar = $descartar > 0 ? $descartar : 0;

    /* Preparando o resultado */
    $torneios = $torneios_model->selecionar_torneios_que_estao_aguardando_inscricoes($filtros, $ordenacao, $quantidade_por_pagina, $descartar);
    $array_torneios = array();

    foreach($torneios as $torneio){
      $array_torneio = array();

      $id = $torneio->get_pk_torneio();
      $array_torneio['id'] = $id;

      $nome = $torneio->get_nome();
      $array_torneio['nome'] = $nome;

      $quantidade_de_waifus = $torneio->get_quantidade_de_waifus();
      $array_torneio['quantidade_de_waifus'] = $quantidade_de_waifus;

      $quantidade_de_vagas_disponiveis = $quantidade_de_waifus - $torneio->get_quantidade_de_waifus_inscritas();
      if($quantidade_de_vagas_disponiveis < 0){
        $quantidade_de_vagas_disponiveis = 0;
      }
      $array_torneio['quantidade_de_vagas_disponiveis'] = $quantidade_de_vagas_disponiveis;

      $array_torneios[] = $array_torneio;
    }
    $valores_deste_metodo['lista'] = $array_torneios;

    return $valores_deste_metodo;
  }

  public function mostrar_torneios_que_estao_aguardando_inscricoes_ajax(){
    $retorno = $this->mostrar_torneios_que_estao_aguardando_inscricoes();
    echo json_encode($retorno);
  }

  private function calcular_quantidade_de_paginas_dos_torneios_que_estao_aguardando_inscricoes($filtros, $quantidade_por_pagina){
    $torneios_model = new TorneiosModel();

    $array_resultado = $torneios_model->contar_torneios_que_estao_aguardando_inscricoes($filtros);
    $quantidade_de_paginas = ceil($array_resultado['quantidade'] / $quantidade_por_pagina);

    return $quantidade_de_paginas;
  }

  private function mostrar_torneios_iniciados(){
    $torneios_model = new TorneiosModel();

    $valores_deste_metodo = array();

    $requisicao = $this->get_requisicao();

    /* Preparando os filtros */
    $filtros = array();
    $nome_do_torneio = trim($requisicao->get('filtro_nome_do_torneio') ?? '');
    if($nome_do_torneio !== ''){
      $filtros['nome_do_torneio'] = $nome_do_torneio;
    }
    $valores_deste_metodo['filtro_nome_do_torneio'] = $nome_do_torneio;

    $inicio_a_partir_de = trim($requisicao->get('filtro_inicio_a_partir_de') ?? '');
    if($inicio_a_partir_de !== ''){
      $momento_inicio = $this->converter_para_data_do_sql($inicio_a_partir_de);
      $filtros['inicio_a_partir_de'] = $momento_inicio;
    }else{
      $inicio_a_partir_de = '';
    }
    $valores_deste_metodo['filtro_inicio_a_partir_de'] = $inicio_a_partir_de;

    $inicio_antes_de = trim($requisicao->get('filtro_inicio_antes_de') ?? '');
    if($inicio_antes_de !== ''){
      $momento_inicio = $this->converter_para_data_do_sql($inicio_antes_de);
      $filtros['inicio_antes_de'] = $momento_inicio;
    }else{
      $inicio_antes_de = '';
    }
    $valores_deste_metodo['filtro_inicio_antes_de'] = $inicio_antes_de;

    /* Preparando a ordenação */
    $ordenacao = $requisicao->get('ordenacao');
    switch($ordenacao){
      case 'padrao':
      case 'nome_a_z':
      case 'nome_z_a':
      case 'inicio_antigos_primeiro':
      case 'inicio_recentes_primeiro':
      case 'quantidade_de_participantes_crescente':
      case 'quantidade_de_participantes_decrescente':
        break;
      default:
        $ordenacao = 'padrao';
        break;
    }
    $valores_deste_metodo['ordenacao'] = $ordenacao;

    /* Preparando a paginação */
    $quantidade_por_pagina = self::QUANTIDADE_PADRAO_POR_PAGINA;

    $pagina = (int) $requisicao->get('pagina');
    if($pagina < 1){
      $pagina = 1;
    }
    $quantidade_de_paginas = $this->calcular_quantidade_de_paginas_dos_torneios_iniciados($filtros, $quantidade_por_pagina);
    if($pagina > $quantidade_de_paginas){
      $pagina = $quantidade_de_paginas;
    }

    $valores_deste_metodo['pagina_atual'] = $pagina;
    $valores_deste_metodo['ultima_pagina'] = $quantidade_de_paginas;

    $descartar = $quantidade_por_pagina * $pagina - $quantidade_por_pagina;
    $descartar = max($descartar, 0);

    /* Preparando o resultado */
    $torneios = $torneios_model->selecionar_torneios_iniciados($filtros, $ordenacao, $quantidade_por_pagina, $descartar);
    $array_torneios = array();
    foreach($torneios as $torneio){
      $array_torneio = array();

      $id = $torneio->get_pk_torneio();
      $array_torneio['id'] = $id;

      $nome = $torneio->get_nome();
      $array_torneio['nome'] = $nome;

      $momento_do_inicio = $torneio->get_momento_do_inicio();

      $sem_fuso_horario = new DateTimeZone('GMT');
      $objeto_date_time = new DateTime($momento_do_inicio, $sem_fuso_horario);

      $fuso_horario_de_brasilia = new DateTimeZone('-0300');
      $objeto_date_time->setTimeZone($fuso_horario_de_brasilia);

      $momento_do_inicio = $objeto_date_time->format('Y-m-d H:i:s');

      $momento_do_inicio = $this->converter_para_horario_data_do_html($momento_do_inicio);
      $array_torneio['momento_do_inicio'] = $momento_do_inicio;

      $quantidade_de_waifus = $torneio->get_quantidade_de_waifus();
      $array_torneio['quantidade_de_waifus'] = $quantidade_de_waifus;

      $array_torneios[] = $array_torneio;
    }

    $valores_deste_metodo['lista'] = $array_torneios;

    return $valores_deste_metodo;
  }

  public function mostrar_torneios_iniciados_ajax(){
    $retorno = $this->mostrar_torneios_iniciados();
    echo json_encode($retorno);
  }

  private function calcular_quantidade_de_paginas_dos_torneios_iniciados($filtros, $quantidade_por_pagina){
    $torneios_model = new TorneiosModel();

    $array_resultado = $torneios_model->contar_torneios_iniciados($filtros);
    $quantidade_de_paginas = ceil($array_resultado['quantidade'] / $quantidade_por_pagina);

    return $quantidade_de_paginas;
  }

  private function mostrar_torneios_encerrados(){
    $torneios_model = new TorneiosModel();

    $valores_deste_metodo = array();

    $requisicao = $this->get_requisicao();

    /* Preparando os filtros */
    $filtros = array();
    $nome_do_torneio = trim($requisicao->get('filtro_nome_do_torneio') ?? '');
    if($nome_do_torneio !== ''){
      $filtros['nome_do_torneio'] = $nome_do_torneio;
    }
    $valores_deste_metodo['filtro_nome_do_torneio'] = $nome_do_torneio;

    $inicio_a_partir_de = trim($requisicao->get('filtro_inicio_a_partir_de') ?? '');
    if($inicio_a_partir_de !== ''){
      $momento_inicio = $this->converter_para_data_do_sql($inicio_a_partir_de);
      $filtros['inicio_a_partir_de'] = $momento_inicio;
    }else{
      $inicio_a_partir_de = '';
    }
    $valores_deste_metodo['filtro_inicio_a_partir_de'] = $inicio_a_partir_de;

    $inicio_antes_de = trim($requisicao->get('filtro_inicio_antes_de') ?? '');
    if($inicio_antes_de !== ''){
      $momento_inicio = $this->converter_para_data_do_sql($inicio_antes_de);
      $filtros['inicio_antes_de'] = $momento_inicio;
    }else{
      $inicio_antes_de = '';
    }
    $valores_deste_metodo['filtro_inicio_antes_de'] = $inicio_antes_de;

    /* Preparando a ordenação */
    $ordenacao = $requisicao->get('ordenacao');
    switch($ordenacao){
      case 'padrao':
      case 'nome_a_z':
      case 'nome_z_a':
      case 'nome_da_vencedora_a_z':
      case 'nome_da_vencedora_z_a':
      case 'inicio_antigos_primeiro':
      case 'inicio_recentes_primeiro':
      case 'quantidade_de_participantes_crescente':
      case 'quantidade_de_participantes_decrescente':
        break;
      default:
        $ordenacao = 'padrao';
        break;
    }
    $valores_deste_metodo['ordenacao'] = $ordenacao;

    /* Preparando a paginação */
    $quantidade_por_pagina = self::QUANTIDADE_PADRAO_POR_PAGINA;

    $pagina = (int) $requisicao->get('pagina');
    if($pagina < 1){
      $pagina = 1;
    }
    $quantidade_de_paginas = $this->calcular_quantidade_de_paginas_dos_torneios_encerrados($filtros, $quantidade_por_pagina);
    if($pagina > $quantidade_de_paginas){
      $pagina = $quantidade_de_paginas;
    }

    $valores_deste_metodo['pagina_atual'] = $pagina;
    $valores_deste_metodo['ultima_pagina'] = $quantidade_de_paginas;

    $descartar = $quantidade_por_pagina * $pagina - $quantidade_por_pagina;
    if($descartar < 0){
      $descartar = 0;
    }

    /* Preparando o resultado */
    $torneios = $torneios_model->selecionar_torneios_encerrados($filtros, $ordenacao, $quantidade_por_pagina, $descartar);
    $array_torneios = array();
    foreach($torneios as $torneio){
      $array_torneio = array();

      $id = $torneio->get_pk_torneio();
      $array_torneio['id'] = $id;

      $nome_do_torneio = $torneio->get_nome();
      $array_torneio['nome_do_torneio'] = $nome_do_torneio;

      $momento_do_inicio = $torneio->get_momento_do_inicio();

      $sem_fuso_horario = new DateTimeZone('GMT');
      $objeto_date_time = new DateTime($momento_do_inicio, $sem_fuso_horario);

      $fuso_horario_de_brasilia = new DateTimeZone('-0300');
      $objeto_date_time->setTimeZone($fuso_horario_de_brasilia);

      $momento_do_inicio = $objeto_date_time->format('Y-m-d H:i:s');

      $momento_do_inicio = $this->converter_para_horario_data_do_html($momento_do_inicio);
      $array_torneio['momento_do_inicio'] = $momento_do_inicio;

      $quantidade_de_waifus = $torneio->get_quantidade_de_waifus();
      $array_torneio['quantidade_de_waifus'] = $quantidade_de_waifus;

      $id_da_waifu_que_venceu = $torneio->get_waifu_vencedora()->get_pk_waifu();
      $array_torneio['id_da_waifu_que_venceu'] = $id_da_waifu_que_venceu;

      $nome_da_waifu_que_venceu = $torneio->get_waifu_vencedora()->get_nome();
      $array_torneio['nome_da_waifu_que_venceu'] = $nome_da_waifu_que_venceu;

      $imagem_da_waifu_que_venceu = $torneio->get_waifu_vencedora()->get_imagem();
      $array_torneio['imagem_da_waifu_que_venceu'] = $imagem_da_waifu_que_venceu;

      $array_torneios[] = $array_torneio;
    }

    $valores_deste_metodo['lista'] = $array_torneios;

    return $valores_deste_metodo;
  }

  public function mostrar_torneios_encerrados_ajax(){
    $retorno = $this->mostrar_torneios_encerrados();
    echo json_encode($retorno);
  }

  private function calcular_quantidade_de_paginas_dos_torneios_encerrados($filtros, $quantidade_por_pagina){
    $torneios_model = new TorneiosModel();

    $array_resultado = $torneios_model->contar_torneios_encerrados($filtros);
    $quantidade_de_paginas = ceil($array_resultado['quantidade'] / $quantidade_por_pagina);

    return $quantidade_de_paginas;
  }

}
