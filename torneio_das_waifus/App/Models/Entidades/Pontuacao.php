<?php

namespace App\Models\Entidades;

final class Pontuacao{
  private $pk_pontuacao;
  private $fk_inscricao_da_waifu;
  private $etapa;
  private $valor;
  private $inscricao;

  public function __construct($array_pontuacao = array()){
    if(isset($array_pontuacao['pk_pontuacao'])){
      $this->pk_pontuacao = $array_pontuacao['pk_pontuacao'];
    }
    if(isset($array_pontuacao['fk_inscricao_da_waifu'])){
      $this->fk_inscricao_da_waifu = $array_pontuacao['fk_inscricao_da_waifu'];
    }
    if(isset($array_pontuacao['etapa'])){
      $this->etapa = $array_pontuacao['etapa'];
    }
    if(isset($array_pontuacao['valor'])){
      $this->valor = $array_pontuacao['valor'];
    }
    if(isset($array_pontuacao['inscricao'])){
      $this->inscricao = $array_pontuacao['inscricao'];
    }
  }

  public function set_pk_pontuacao($pk_pontuacao){
    $this->pk_pontuacao = $pk_pontuacao;
  }

  public function set_fk_inscricao_da_waifu($fk_inscricao_da_waifu){
    $this->fk_inscricao_da_waifu = $fk_inscricao_da_waifu;
  }

  public function set_etapa($etapa){
    $this->etapa = $etapa;
  }

  public function set_valor($valor){
    $this->valor = $valor;
  }

  public function set_inscricao($inscricao){
    $this->inscricao = $inscricao;
  }

  public function get_pk_pontuacao(){
    return $this->pk_pontuacao;
  }

  public function get_fk_inscricao_da_waifu(){
    return $this->fk_inscricao_da_waifu;
  }

  public function get_etapa(){
    return $this->etapa;
  }

  public function get_valor(){
    return $this->valor;
  }

  public function get_inscricao(){
    return $this->inscricao;
  }

}
