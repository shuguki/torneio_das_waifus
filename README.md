## Sobre
<p>Este sistema permite criar torneios do tipo eliminatórias (playoffs), popularmente conhecidos como "mata-mata", com suas personagens femininas favoritas ("waifus").</p>
<p>Crie torneios finais das waifus mais famosas com este sistema, é possível criar com até 16 avos de final, ou seja 32 waifus competindo em um único torneio.</p>

<p>As tecnologias que utilizei para desenvolver este sistema foram:<br/>
PHP 8<br/>
Laravel 9<br/>
Vite 3<br/>
Vue 3<br/>
Inertia-Laravel 0.6.3</p>

<p>Este sistema foi testado em:<br/>
MySQL Server 5.7<br/>
MySQL Server 8.0<br/>
Servidor Apache 2.4.54<br/>
Servidor Apache 2.4.58<br/>
</p>

<p>Este sistema foi feito por mim, mas qualquer pessoa é livre para reutilizar e/ou modificar.</p>

<br/>

## Instruções
<p>Para ver o resultado em um ambiente de desenvolvimento siga as instruções:</p>

<p>Inicie o MySQL Server.</p>

<p>Utilize o banco de dados contido no arquivo banco_de_dados_torneio_das_waifus.sql. As tabelas são necessárias e os dados de exemplo são opcionais.</p>

<p>Configure o MySQL Server para que o banco de dados deste sistema seja acessado por username root sem senha.</p>

<p>Se você preferir, você pode configurar neste sistema um outro username e uma outra senha.</p>

<p>Configure seu PHP 8 pelo arquivo php.ini e certifique-se de deixar ativado intl e mbstring.</p>

<p>Coloque o diretório torneio_das_waifus dentro do endereço DocumentRoot do seu Servidor Apache. Exemplo: coloque dentro de htdocs do XAMPP. Geralmente o DocumentRoot é o diretório htdocs do XAMPP e você pode consultar ou mudar o endereço de DocumentRoot pelo arquivo de configuração do Servidor Apache (exemplo: arquivo httpd.conf).</p>

<p>Configure um VirtualHost no Servidor Apache para este sistema.<br/>
Dica: configure com a porta 80 e ServerName localhost ou, se tiver dúvida, faça conforme o manual do Laravel 9.<br/>
Se utiliza XAMPP, o arquivo de configuração do Servidor Apache para VirtualHost será apache\conf\extra\httpd-vhosts.conf<br/>
Exemplo de VirtualHost configurado:<br/>
<code>&lt;VirtualHost *:80&gt;</code><br/>
<code>&nbsp;&nbsp;DocumentRoot "C:\Users\meu_nome_verdadeiro\Servidores\xampp003\htdocs\torneio_das_waifus\public"</code><br/>
<code>&nbsp;&nbsp;ServerName localhost</code><br/>
<code>&lt;/VirtualHost&gt;</code></p>

<p>Inicie ou reinicie o Servidor Apache.</p>

<p>Dentro do diretório torneio_das_waifus execute os comandos:<br/>
composer install<br/>
npm install</p>

<p>Renomeie o arquivo .env.example para .env<br/>
Gere a chave APP_KEY pelo comando: php artisan key:generate<br/>
Execute os comandos:<br/>
npm run build<br/>
npm run dev</p>

<p>Ainda dentro do diretório torneio_das_waifus, com outra janela de terminal, execute o comando:<br/>
php artisan schedule:work</p>

<p>Acesse o endereço http://localhost:80 em um navegador.</p>

<br/>
