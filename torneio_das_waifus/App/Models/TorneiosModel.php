<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use App\Models\Entidades\Torneio;
use App\Models\Entidades\Waifu;

final class TorneiosModel{

  public function selecionar_torneios_que_estao_aguardando_inscricoes($filtros, $ordenacao, $quantidade, $descartar){
    $query = DB::table('torneio');
    $query = $query->select(
      'pk_torneio',
      'nome',
      'quantidade_de_waifus',
      DB::raw('(SELECT COUNT(*) FROM inscricao_da_waifu WHERE fk_torneio = pk_torneio) AS quantidade_de_waifus_inscritas')
    );
    $query = $query->where('status', '=', 'aguardando_inscricoes');

    foreach($filtros as $chave => $valor){
      switch($chave){
        case 'nome_do_torneio':
          $query = $query->where('nome', 'LIKE', "%$valor%");
          break;
      }
    }

    switch($ordenacao){
      case 'padrao':
        $query = $query->orderBy('pk_torneio', 'DESC');
        break;
      case 'nome_a_z':
        $query = $query->orderBy('nome', 'ASC');
        break;
      case 'nome_z_a':
        $query = $query->orderBy('nome', 'DESC');
        break;
      case 'vagas_disponiveis_crescente':
        $query = $query->orderByRaw('IF(CAST(CAST(quantidade_de_waifus AS CHAR) AS UNSIGNED) - quantidade_de_waifus_inscritas < 0, 0, CAST(CAST(quantidade_de_waifus AS CHAR) AS UNSIGNED) - quantidade_de_waifus_inscritas) ASC');
        $query = $query->orderBy('pk_torneio', 'DESC');
        break;
      case 'vagas_disponiveis_decrescente':
        $query = $query->orderByRaw('IF(CAST(CAST(quantidade_de_waifus AS CHAR) AS UNSIGNED) - quantidade_de_waifus_inscritas < 0, 0, CAST(CAST(quantidade_de_waifus AS CHAR) AS UNSIGNED) - quantidade_de_waifus_inscritas) DESC');
        $query = $query->orderBy('pk_torneio', 'DESC');
        break;
      case 'total_de_vagas_crescente':
        $query = $query->orderBy('quantidade_de_waifus', 'ASC');
        $query = $query->orderBy('pk_torneio', 'DESC');
        break;
      case 'total_de_vagas_decrescente':
        $query = $query->orderBy('quantidade_de_waifus', 'DESC');
        $query = $query->orderBy('pk_torneio', 'DESC');
        break;
    }

    $query = $query->offset($descartar);
    $query = $query->limit($quantidade);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado = array();
    foreach($array_resultado as $objeto_generico){
      $array_torneio = (array) $objeto_generico;
      $torneio = new Torneio($array_torneio);
      $array_melhorado[] = $torneio;
    }
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

  public function contar_torneios_que_estao_aguardando_inscricoes($filtros){
    $query = DB::table('torneio');
    $query = $query->select(DB::raw('COUNT(*) AS quantidade'));
    $query = $query->where('status', '=', 'aguardando_inscricoes');

    foreach($filtros as $chave => $valor){
      switch($chave){
        case 'nome_do_torneio':
          $query = $query->where('nome', 'LIKE', "%$valor%");
          break;
      }
    }

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado['quantidade'] = $array_resultado[0]->quantidade;
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

  public function selecionar_torneios_iniciados($filtros, $ordenacao, $quantidade, $descartar){
    $query = DB::table('torneio');
    $query = $query->addSelect('pk_torneio');
    $query = $query->addSelect('nome');
    $query = $query->addSelect('momento_do_inicio');
    $query = $query->addSelect('quantidade_de_waifus');
    $query = $query->where('status', '=', 'torneio_iniciado');

    foreach($filtros as $chave => $valor){
      switch($chave){
        case 'nome_do_torneio':
          $query = $query->where('nome', 'LIKE', "%$valor%");
          break;
        case 'inicio_a_partir_de':
          $query = $query->where('momento_do_inicio', '>=', $valor);
          break;
        case 'inicio_antes_de':
          $query = $query->where('momento_do_inicio', '<', $valor);
          break;
      }
    }

    switch($ordenacao){
      case 'padrao':
        $query = $query->orderBy('momento_do_inicio', 'DESC');
        $query = $query->orderBy('pk_torneio', 'DESC');
        break;
      case 'nome_a_z':
        $query = $query->orderBy('nome', 'ASC');
        break;
      case 'nome_z_a':
        $query = $query->orderBy('nome', 'DESC');
        break;
      case 'inicio_antigos_primeiro':
        $query = $query->orderBy('momento_do_inicio', 'ASC');
        $query = $query->orderBy('pk_torneio', 'DESC');
        break;
      case 'inicio_recentes_primeiro':
        $query = $query->orderBy('momento_do_inicio', 'DESC');
        $query = $query->orderBy('pk_torneio', 'DESC');
        break;
      case 'quantidade_de_participantes_crescente':
        $query = $query->orderBy('quantidade_de_waifus', 'ASC');
        $query = $query->orderBy('momento_do_inicio', 'DESC');
        $query = $query->orderBy('pk_torneio', 'DESC');
        break;
      case 'quantidade_de_participantes_decrescente':
        $query = $query->orderBy('quantidade_de_waifus', 'DESC');
        $query = $query->orderBy('momento_do_inicio', 'DESC');
        $query = $query->orderBy('pk_torneio', 'DESC');
        break;
    }

    $query = $query->offset($descartar);
    $query = $query->limit($quantidade);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado = array();
    foreach($array_resultado as $objeto_generico){
      $array_torneio = (array) $objeto_generico;
      $torneio = new Torneio($array_torneio);
      $array_melhorado[] = $torneio;
    }
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

  public function contar_torneios_iniciados($filtros){
    $query = DB::table('torneio');
    $query = $query->select(DB::raw('COUNT(*) AS quantidade'));
    $query = $query->where('status', '=', 'torneio_iniciado');

    foreach($filtros as $chave => $valor){
      switch($chave){
        case 'nome_do_torneio':
          $query = $query->where('nome', 'LIKE', "%$valor%");
          break;
        case 'inicio_a_partir_de':
          $query = $query->where('momento_do_inicio', '>=', $valor);
          break;
        case 'inicio_antes_de':
          $query = $query->where('momento_do_inicio', '<', $valor);
          break;
      }
    }

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado['quantidade'] = $array_resultado[0]->quantidade;
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

  public function selecionar_torneios_encerrados($filtros, $ordenacao, $quantidade, $descartar){
    $query = DB::table('torneio');
    $query = $query->addSelect('pk_torneio');
    $query = $query->addSelect('torneio.fk_vencedora');
    $query = $query->addSelect('torneio.nome AS nome_do_torneio');
    $query = $query->addSelect('torneio.momento_do_inicio');
    $query = $query->addSelect('torneio.quantidade_de_waifus');
    $query = $query->addSelect('waifu.nome AS nome_da_waifu');
    $query = $query->addSelect('waifu.imagem AS imagem_da_waifu');
    $query = $query->where('status', '=', 'torneio_encerrado');
    $query = $query->leftJoin('waifu', 'fk_vencedora', '=', 'pk_waifu'); //LEFT JOIN

    foreach($filtros as $chave => $valor){
      switch($chave){
        case 'nome_do_torneio':
          $query = $query->where('torneio.nome', 'LIKE', "%$valor%");
          break;
        case 'inicio_a_partir_de':
          $query = $query->where('torneio.momento_do_inicio', '>=', $valor);
          break;
        case 'inicio_antes_de':
          $query = $query->where('torneio.momento_do_inicio', '<', $valor);
          break;
      }
    }

    switch($ordenacao){
      case 'padrao':
        $query = $query->orderBy('torneio.momento_do_inicio', 'DESC');
        $query = $query->orderBy('pk_torneio', 'DESC');
        break;
      case 'nome_a_z':
        $query = $query->orderBy('torneio.nome', 'ASC');
        break;
      case 'nome_z_a':
        $query = $query->orderBy('torneio.nome', 'DESC');
        break;
      case 'nome_da_vencedora_a_z':
        $query = $query->orderBy('waifu.nome', 'ASC');
        break;
      case 'nome_da_vencedora_z_a':
        $query = $query->orderBy('waifu.nome', 'DESC');
        break;
      case 'inicio_antigos_primeiro':
        $query = $query->orderBy('torneio.momento_do_inicio', 'ASC');
        $query = $query->orderBy('pk_torneio', 'DESC');
        break;
      case 'inicio_recentes_primeiro':
        $query = $query->orderBy('torneio.momento_do_inicio', 'DESC');
        $query = $query->orderBy('pk_torneio', 'DESC');
        break;
      case 'quantidade_de_participantes_crescente':
        $query = $query->orderBy('torneio.quantidade_de_waifus', 'ASC');
        $query = $query->orderBy('torneio.momento_do_inicio', 'DESC');
        $query = $query->orderBy('pk_torneio', 'DESC');
        break;
      case 'quantidade_de_participantes_decrescente':
        $query = $query->orderBy('torneio.quantidade_de_waifus', 'DESC');
        $query = $query->orderBy('torneio.momento_do_inicio', 'DESC');
        $query = $query->orderBy('pk_torneio', 'DESC');
        break;
    }

    $query = $query->offset($descartar);
    $query = $query->limit($quantidade);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado = array();
    foreach($array_resultado as $objeto_generico){
      $array_valores_do_banco_de_dados = (array) $objeto_generico;
      $torneio = new Torneio($array_valores_do_banco_de_dados);
      $torneio->set_nome($array_valores_do_banco_de_dados['nome_do_torneio']);

      $waifu = new Waifu();
      $waifu->set_pk_waifu($array_valores_do_banco_de_dados['fk_vencedora']);
      $waifu->set_nome($array_valores_do_banco_de_dados['nome_da_waifu']);
      $waifu->set_imagem($array_valores_do_banco_de_dados['imagem_da_waifu']);

      $torneio->set_waifu_vencedora($waifu);

      $array_melhorado[] = $torneio;
    }
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

  public function contar_torneios_encerrados($filtros){
    $query = DB::table('torneio');
    $query = $query->select(DB::raw('COUNT(*) AS quantidade'));
    $query = $query->where('status', '=', 'torneio_encerrado');

    foreach($filtros as $chave => $valor){
      switch($chave){
        case 'nome_do_torneio':
          $query = $query->where('torneio.nome', 'LIKE', "%$valor%");
          break;
        case 'inicio_a_partir_de':
          $query = $query->where('torneio.momento_do_inicio', '>=', $valor);
          break;
        case 'inicio_antes_de':
          $query = $query->where('torneio.momento_do_inicio', '<', $valor);
          break;
      }
    }

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado['quantidade'] = $array_resultado[0]->quantidade;
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

}
